;; INCF is a macro
;; what does it do?

(defparameter foo 1)

(defmacro ppmx (form)
  "pretty prints macro expansion of FORM"
  `(let* ((exp1 (macroexpand-1 ',form))
	  (exp (macroexpand exp1))
	  (*print-circle* nil))
     (cond ((equal exp exp1)
	    (format t "~&Macro expansion:")
	    (pprint exp))
	   (t (format t "~&First step of expansion:")
	      (pprint exp1)
	      (format t "~%~%Final expansion:")
	      (pprint exp)))
     (format t "~%~%")
     (values)))

(ppmx (setf foo 2))
(ppmx (incf foo 10))
;; Macro expansion:
;; (SETQ FOO (+ 10 FOO))

;; 14.1
(ppmx (pop x))
;; First step of expansion:
;; (PROG1 (CAR X) (SETQ X (CDR X)))
;; 
;; Final expansion:
;; (LET ((#:G620 (CAR X)))
;; ;;   (PROGN (SETQ X (CDR X)) #:G620))

(ppmx (prog1 test))
;; Macro expansion:
;; (LET ((#:G621 TEST))
;; ;;   (PROGN #:G621))

(ppmx (prog2 'foo 'bar 'baz))
;; First step of expansion:
;; (PROG1 (PROGN 'FOO 'BAR) 'BAZ)
;; 
;; Final expansion:
;; (LET ((#:G622 (PROGN 'FOO 'BAR)))
;; ;;   (PROGN 'BAZ #:G622))

(ppmx (progn 'foo 'bar 'bax 'baz 'heha))
;; Macro expansion:
;; (PROGN 'FOO 'BAR 'BAX 'BAZ 'HEHA)

;; 14.2
(ppmx (defstruct starship
	(name nil)
	(condition 'green)))
;; Macro expansion:
;; it's like 70 lines of code

;; macros are defined by DEFMACRO
;; Defining it like THIS doesn't work -- why?
;; because macros generate lisp code -- they don't evaluate it.
;; (defmacro sinple-incf (a)
;; ;;   (setq a (+ a 1)))

(defmacro sinple-incf (var)
  `(setq ,var (+ ,var 1)))

;; (ppmx (sinple-incf f))
;; Macro expansion:
;; (SETQ F (+ F 1))


(defmacro extra-fancy-incf (var &optional (delta 1))
  "constructs a statement to setq var to (+ var delta)"
  `(setq ,var (+ ,var ,delta)))

(ppmx (extra-fancy-incf f 10))
;; Macro expansion:
;; (SETQ F (+ F 10))

;; SETQ is not a macro; it is a SPECIAL FUNCTION
;; what's the difference?

;; 14.3

(defmacro set-nil (var)
  "constructs an expression to set a given var to nil"
  `(setq ,var nil))

;; 14.5 - MACROS AS SYNTACTIC EXPRESSIONS

;; 1. The arguments to ordinary functions are always evaluated
;;  - the arguments to macro functions are NOT EVALUATED 

;; 2. The result of an ordinary functino can be anything at all
;;  - THe result of a macro function must be a VALID LISP EXPRESSION

;; 3. After a macro function returns an expression, that expression
;;        IS IMMEDIATELY VALIDATED
;;   - The results of an ordinary function do not get evaluated

;; Anything that can be done as a macro can be done without macros
;;   with some combination of ordinary common lisp functions,
;;   special functions, and maybe some implementation specific dependent functions
;; but just imagine typing in what
;; (ppmx (defstruct starship
;; ;; 	(name nil)
;; ;; 	(condition 'green)))
;; gives you

;; 14.4
(defmacro simple-rotatef (var1 var2)
  ;; so, whenever we want to reference temp-sym, we need to use ,
  (let ((temp-sym (gensym)))
    `(let ((,temp-sym ,var1))
       (setf ,var1 ,var2)
       (setf ,var2 ,temp-sym))))

(defparameter f 10)
(defparameter g "hella fella")

;; before building the guts out
;; (ppmx (simple-rotatef f g))
;; Macro expansion:
;; (LET ((#:G631 F)))

(ppmx (simple-rotatef f g))
;; Macro expansion:
;; (LET ((#:G632 F))
;; ;;   (SETF F G)
;; ;;   (SETF G #:G632))

;; 14.5
(defmacro set-mutual (var1 var2)
  ;; so why does this work? well
  ;; ,var2 expands to the actual value passed in for var2
  ;; however ... the quote means that the "actual value" expands to a quoted expr
  `(progn (setf ,var1 ',var2)
	  (setf ,var2 ',var1)))

(ppmx (set-mutual a b))
;; Macro expansion:
;; (PROGN (SETF A 'B) (SETF B 'A))

(defmacro showvar (var)
  `(format t "~&The value of ~S is ~A" ',var ,var))

(showvar a)
;; why do we have to do this with a macro?
;; well -- ordinary functions DONT KNOW the names of the variables provided as arguments
(ppmx (showvar f))
;; Macro expansion:
;; (FORMAT T "~&The value of ~S is ~A" 'F F)

;; list-splicing backquotes
(defparameter name 'fred)
(defparameter address '(16 maple drive))

`(,name lives at ,address now)
;; quasiquote inserting
;;=>(FRED LIVES AT (16 MAPLE DRIVE) NOW)

`(,name lives at ,@address now)
;; quasiquote SPLICING
;; (FRED LIVES AT 16 MAPLE DRIVE NOW)

(defmacro set-zero (&rest args)
  "sets every argument to zero"
  `(progn ,@(mapcar (lambda (arg)
		      `(setf ,arg 0))
		    args)
	  '(zeroed ,@args)))

;; 14.6
(defmacro variable-parade (fst &rest rest-args
			   &aux (args (cons fst rest-args)))
  (let ((tail (cdr args)))
    `(progn ,@(mapcar (lambda (arg val)
			`(setf ,arg ',val))
		      args tail)
	    ',args)))
