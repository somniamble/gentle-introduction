(defstruct (node (:print-function print-node))
  (name nil)
  (inputs nil)
  (outputs nil))

(defun print-node (node stream depth)
  (format stream "#<Node ~A>"
	  (node-name node)))

;;; from - node where the arc originates
;;; to - node where the arc ends
;;; label - name of the arc
;;; action - the action associated with the arc
(defstruct (arc (:print-function print-arc))
  (from nil)
  (to nil)
  (label nil)
  (action nil))

(defun print-arc (arc stream depth)
  (format stream "#<ARC ~A / ~A / ~A>"
	  (node-name (arc-from arc))
	  (arc-label arc)
	  (node-name (arc-to arc))))

(defvar *nodes*)
(defvar *arcs*)
(defvar *current-node*)

(defun initialize ()
  (setf *nodes* nil)
  (setf *arcs* nil)
  (setf *current-node* nil))

(defmacro defnode (name)
  `(add-node ',name))

(defun add-node (name)
  "Adds the node to the end of the list of nodes"
  (let ((new-node (make-node :name name)))
    (setf *nodes* (nconc *nodes* (list new-node)))
    new-node))

(defun find-node (name)
  "Finds a node by node-name in the list of nodes, or errors"
  (or (find name *nodes* :key #'node-name)
      (error "No node named ~A exists." name)))

(defmacro defarc (from label to &optional action)
  `(add-arc ',from ',label ',to ',action))

(defmacro push-end (x lst)
  "modifies the list in-place by destructively concatenating x onto the end"
  `(setf ,lst (nconc ,lst (list ,x))))

(defun add-arc (from-name label to-name action)
  (let* ((from (find-node from-name))
	 (to (find-node to-name))
	 (new-arc (make-arc :from from
			    :label label
			    :to to
			    :action action)))
    (push-end new-arc *arcs*)
    (push-end new-arc (node-outputs from))
    (push-end new-arc (node-inputs to))
    new-arc))

(defun fsm (&optional (starting-point 'start))
  (setf *current-node* (find-node starting-point))
  (do ()
      ((null (node-outputs *current-node*)))
    (one-transition)))


(progn (initialize)
       (defnode start)
       (defnode have-5)
       (defnode have-10)
       (defnode have-15)
       (defnode have-20)
       (defnode have-25)
       (defnode end)

       (defarc start dime have-10 "Clink!")
       (defarc start nickel have-5 "Clunk!")
       (defarc start quarter have-25 "Ker-Chunk!!")
       (defarc start coin-return start "No coins to return")

       (defarc have-5 nickel have-10 "Clunk!")
       (defarc have-5 dime have-15 "Clink!")
       (defarc have-5 coin-return start "Returned a nickel")
       
       (defarc have-10 nickel have-15 "Clunk!")
       (defarc have-10 dime have-20 "Clink!")
       (defarc have-10 coin-return start "Returned a dime")
       
       (defarc have-15 nickel have-20 "Clunk!")
       (defarc have-15 gum-button end "Deliver Gum")
       (defarc have-15 coin-return start "Returned a nickel and a dime")
       (defarc have-15 dime have-25 "Clink!")
       
       (defarc have-20 nickel have-25 "Clunk!")
       (defarc have-20 gum-button end "Deliver Gum, Nickel Change")
       (defarc have-20 mints-button end "Deliver mints")
       (defarc have-20 coin-return start "Returned a couple a dimes")

       (defarc have-25 coin-return start "have a quarter")
       (defarc have-25 kit-kat end "give me a break etc"))

(defun one-transition ()
  ;; output current node name, current state
  (format t "~&State ~a. Input: "
	  (node-name *current-node*))
  ;; an arc from the user, see if it's to be found in the outputs of *current-node*
  (let* ((ans (read))
	 (arc (find ans
		    (node-outputs *current-node*)
		    :key #'arc-label)))
    ;; If the arc isn't found, notify the user and return. I would do a progn...
    (unless arc
      (format t "~&No arc from ~A has label ~a.~%"
	      (node-name *current-node*) ans)
      (return-from one-transition nil))
    ;; or, update the current node to the arc's destination, and give the user a jingle
    (let ((new (arc-to arc)))
      (format t "~&~A" (arc-action arc))
      (setf *current-node* new))))

;; 14.8
;; side effects in the body of a macro (e.g. a format statement) might be
;;   factored out by the compiler, or they will run at unpredictable times
;;   (i,e, whenever the compiler feels like it.)

;; 14.9
;; all common lisp special forms:
;; block
;; if
;; progv
;; catch
;; labels
;; quote
;; compiler-let
;; let
;; let*
;; setq
;; eval-when
;; macrolet
;; tagbody
;; flet
;; multiple-value-call
;; the
;; function
;; multiple-value-prog1
;; throw
;; go
;; progn
;; unwind-protect


;; 14.10
;; a damn sight faster. It really depends on the language and the compiler optimizations though
;; for example, if I'm recursively generating (* x1 (* x2 (* ..))) calls then
;; the compiler might factor that into lower-level instructions that
;;  don't rely on consing

;; not compiling this since i don't have those functions defined...
#+nil(defun start (input-syms &aux (this-input (first input-syms)))
  (cond ((null input-syms) 'start)
	((equal this-input 'nickel)
	 (format t "~&~A" "Clunk!")
	 (have-5 (rest input-syms)))
	((equal this-input 'dime)
	 (format t "~&~A" "Cink!")
	 (have-10 (rest input-syms)))
	((equal this-input 'coin-return)
	 (format t "~&~A" "Nothing to return")
	 (start (rest input-syms)))
	(t (error "No arc from ~A with label ~A."
		  'start this-input))))

;; a
(defun compile-arc (arc)
  "Compiles an arc into a cond statement for a compiled node"
  (let ((label (arc-label arc))
	(action (arc-action arc))
	(nodename (node-name (arc-to arc))))
    `((equal this-input ',label)
      (format t "~&~A" ,action)
      (,nodename (rest input-syms)))))
;; b.

(defun compile-node (node)
  (let ((name (node-name node))
	(outs (node-outputs node)))
    `(defun ,name (input-syms &aux (this-input (first input-syms)))
       (cond ((null input-syms) ',name)
	     ,@(mapcar #'compile-arc outs)
	     (t (error "No arc from ~A with label ~A."
		       ',name this-input))))))

;; c
;; this gave me the hardest time
;; I kept thinking I was doing something wrong.... but actually
;; you ARE supposed to evaluate the macro argument, otherwise it's just the literal variable you pass in
(defmacro compile-machine (nodes)
  `(progn ,@(mapcar #'compile-node (eval nodes))))


;; (defmacro compile-arc (x)
;; ;;   `,x)
