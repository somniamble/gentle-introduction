;; oh here's something cool
;; checkout this sickas while macro

(defmacro while (test &body body)
  `(do ()
    ((not ,test))
    ,@body))

;; destructuring lambda lists
(defmacro swinger-party ((hus1 wife1) (hus2 wife2))
  `(list '(,hus1 ,wife2)
	 '(,hus2 ,wife1)))

;; (swinger-party (fred wilma) (barney betty))
;; ((FRED BETTY) (BARNEY WILMA))

;; so, we specify the variable that we step through the 
(defmacro dovector ((var vector-exp
		     &optional result-form)
		    &body body)
  (let ((vec (gensym))
	(len (gensym))
	(i (gensym)))
    `(do* ((,vec ,vector-exp)
	   (,len (length ,vec))
	   (,i 0 (+ ,i 1))
	   (,var nil ))
	  ((equal ,i ,len) ,result-form)
       (setf ,var (aref ,vec ,i))
       ,@body)))
;; we can use X in the body of dovector since it's what we're specifying at the top
;; isn't that neat?
;; (dovector (x '#(foo bar bax))
;; ;;   (format t "~&X is ~S" x))
;; X is FOO
;; X is BAR
;; X is BAX
;; 
;; NIL

;; special variables:
(defvar *a-variable-which-is-expected-to-be-changed* 'defvared)

(defparameter *a-variable-which-is-expected-to-remain-the-same* 'defparamed)

(defconstant a-constant-value-with-a-name-that-is-forbidden-to-change 'defconsted)
