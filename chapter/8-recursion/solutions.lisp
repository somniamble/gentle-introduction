#+nil
(defun anyoddp (x)
  (cond ((null x) nil)
	((oddp (first x)) t)
	(t (anyoddp (rest x)))))

(defun anyoddp (x)
  (if x (if (oddp (first x))
	    t
	    (anyoddp (rest x)))))

;; (anyoddp '(3142 5789 6550 8914))
;; 0: (ANYODDP (3142 5789 6550 8914))
  ;; 1: (ANYODDP (5789 6550 8914))
  ;; 1: ANYODDP returned T
;; 0: ANYODDP returned T
;; in this case, (null x) is never T, since we halt on the list's second term

(defun fact (n)
  (cond ((zerop n) 1)
	(t (* n (fact (- n 1))))))

;; (fact 20) is integer, whole number is displayed
;; (fact 20)
;; 2432902008176640000
;; (fact 20.0) is floating, and due to the size is displayed in scientific notation
;; (fact 20.0)
;; 2.432902e18

;; (fact 0) and (fact 0.0) both satisfy the first clause, that n == 0

;; 8.5
;; a.
;; We stop when the list is empty, and return 0. (i.e. we know the empty list has value 0)
;; b. we take a single step by  popping the first value off and adding it -- N + ...
;; c. (add-up (cdr lst))

(defun add-up (lst)
  (if (null lst) 0
      (+ (car lst) (add-up (cdr lst)))))

;; 8.6
(defun alloddp (lst)
  (cond ((null lst) t)
	((evenp (car lst)) nil)
	(t (alloddp (cdr lst)))))

;; 8.7
(defun rec-member (x lst)
  (cond ((null lst) nil)
	((equal x (car lst)) (car lst))
	(t (rec-member x (cdr lst)))))

;; 8.8
(defun rec-assoc (x lst)
  (cond ((null lst) nil)
	((equal x (caar lst)) (car lst))
	(t (rec-assoc x (cdr lst)))))

;; 8.9
(defun rec-nth (n lst)
  (cond ((null lst) nil)
	((= n 0) (car lst))
	(t (rec-nth (- n 1) (cdr lst)))))

;; 8.10
(defun add1 (n) (+ n 1))

(defun sub1 (n) (- n 1))

(defun rec-plus (x y)
  (if (= 0 y) x
      (rec-plus (add1 x) (sub1 y))))

;; 8.11
(defun fib (n)
  (cond ((= n 0) 1)
	((= n 1) 1)
	(t (+ (fib (- n 1))
	      (fib (- n 2))))))

;; 8.12
(defun any-7-p (x)
  (cond ((equal (first x) 7) t)
	(t (any-7-p (rest x)))))
;; (any-7-p '(1 2 3 45 7 8 9))
;; T
;; (any-7-p '(1 2 3))
;; => infinite loop
;; there's never a check for the empty list

;; 8.13
;; FACT will recur infinitely if given a negative input

;; 8.14
#+nil
(defun r () (r))

;; 8.15
;; the car of the circular list (x #) is x
;;  the cdr of the circular list (x #) is (x #)
;;  if given to COUNT-SLICES, the function will recur infinitely

;; recursion templates, double test tail and single-test tail


;; 8.16
;; if we switch the conditions around in anyoddp we get an error if we ever encounter (oddp nil)

;; 8.17
(defun find-first-odd (lst)
  (cond ((null lst) nil)
	((oddp (car lst)) (car lst))
	(t (find-first-odd (cdr lst)))))

(defun last-element (lst)
  (if (null (cdr lst))
      (car lst)
      (last-element (cdr lst))))

(defun addnums (n)
  (if (zerop n) 0
      (+ n (addnums (- n 1)))))

(defun all-equal (lst)
  (cond ((null (cdr lst)) t)
	((not (equal (car lst) (cadr lst))) nil)
	(t (all-equal (cdr lst)))))

;; 8.23
(defun laugh (n)
  (if (zerop n) nil
      (cons 'ha (laugh (- n 1)))))

;; 8.24
(defun count-down (n)
  (if (zerop n) nil
      (cons n (count-down (- n 1)))))

(count-down 5)

;; 8.25
(defun app-fact (n)
  (reduce #'* (count-down n)))


;; 8.26
(defun count-down-to-zero1 (n)
  (if (zerop n) '(0)
      (cons n (count-down-to-zero1 (- n 1)))))

(defun count-down-to-zero2 (n)
  (if (< n 0) nil
      (cons n (count-down-to-zero2 (- n 1)))))


;; 8.27
(defun square-list (lst)
  (if (null lst) nil
      (cons (* (car lst) (car lst)) (square-list (cdr lst)))))

;;; 8.28
(defun my-nth-bad (n x)
  (cond ((zerop n) (first x))
	(t (my-nth-bad (- n 1) (rest x)))))

(defun my-nth (n x)
  (cond ((or (zerop n) (null x)) (first x))
	(t (my-nth (- n 1) (rest x)))))


;;; 8.29
(defun my-member (m lst)
  (cond ((null lst) nil)
	((equal m (car lst)) lst)
	(t (my-member m (cdr lst)))))


;;; 8.30
(defun my-assoc (k alist)
  (cond ((null alist) nil)
	((equal k (caar alist)) (car alist))
	(t (my-assoc k (cdr alist)))))

;;; 8.31
(defun compare-lengths (l1 l2)
  (if (null l1)
      ;; l1 is null
      (if (null l2)
	  ;; if l2 is also null, lists are the same length
	  'SAME-LENGTH
	  ;; if l2 is not null, then we know that l2 is the longer list 
	  'SECOND-IS-LONGER)
      ;; l1 is NOT NULL
      (if (null l2)
	  ;; if l2 is null, then we know that l1 is the longer list
	  'FIRST-IS-LONGER
	  ;; otherwise, we recur on the tails of both lists
	  (compare-lengths (cdr l1) (cdr l2)))))

;;; 8.32
(defun sum-numeric-elements (lst)
  "Takes a list and returns the sum of all numeric elements of the list"
  (labels ((number-or-zero (x)
	   (if (numberp x) x 0))
	 (sumup (l acc)
	   (if (null l)
	       acc
	       (sumup (cdr l) (+ acc (number-or-zero (car l)))))))
    (sumup lst 0)))

;;; 8.33
(defun my-remove (x lst)
  "removes all instances of the given items from a list"
  (cond ((null lst) nil)
	((equal x (car lst)) (my-remove x (cdr lst)))
	(t (cons (car lst) (my-remove x (cdr lst))))))


;;; 8.34
(defun my-intersection (list1 list2)
  "performs a set intersecion upon two lists"
  ;; first condition, either list1 or list2 are null, so the intersection is the empty set
  (cond ((or (null list1) (null list2)) nil)
	;; second condition, the car of list2 is a member of list1, so the intersection is
	;;   the car of list2 and the intersection of (cdr list2) and list1
	((member (car list2) list1)
	 (cons (car list2) (my-intersection list1 (cdr list2))))
	;; third condition, the car of list2 is not a member of list1, so it is not included in the intersection
	(t (my-intersection list1 (cdr list2)))))

;; 8.35
(defun my-set-difference (list1 list2)
  "performs a set intersecion upon two lists"
  ;; first condition, list1 is null, so the difference is nil
  (cond ((null list1) nil)
	;; second condition, list2 is null, so the difference is just list1
	((null list2) list1)
	;; third condition, (car list1) is a member of list2, so it is not included in the difference
	((member (car list1) list2) (my-set-difference (cdr list1) list2))
	;; fourth condition, (car list1) is not a member of list2, so it is included in the difference
	(t (cons (car list1) (my-set-difference (cdr list1) list2)))))


;; 8.36
;; conditional augmentation -- only augment the recurred value if we satisfy augtest
(defun count-odd-condaug (lst)
  (cond ((null lst) 0)
	((oddp (car lst))
	 (+ 1 (count-odd-condaug (cdr lst))))
	(t (count-odd-condaug (cdr lst)))))

;; regular augmentation -- apply the augmenting function each recursion.
;; note that in this case our aug-value is more sophisticated
(defun count-odd-regaug (lst)
  (if (null lst) 0
      ;; first version, for scrubs
      ;; (if (oddp (car lst)) 1 0)
      (+ (mod (car lst) 2)
	 (count-odd-regaug (cdr lst)))))


;; multi-recursion (more than one recursive call per invocation)
;; 8.37
(defun combine (x y)
  (+ x y))

(defun fib (n)
  (cond ((equal n 0) 1)
	((equal n 1) 1)
	(t (combine (fib (- n 1))
		    (fib (- n 2))))))

;; it appears that combine is only ever called on non-terminal calls.


;; 8.12
;; multiple-recursion
#+nil(defun func (n)
  (cond (end-test-1 end-value-1)
	(end-test-2 end-value-2)
	(t (combiner (func first-reduced-n)
		     (func second-reduced-n)))))
;; 8.13
;; car/cdr recursion
;; a special case of multiple-recursion
#+nil(defun func (n)
  (cond (end-test-1 end-value-1)
	(end-test-2 end-value-2)
	(t (combiner (func (car n))
		     (func (cdr n))))))
;; example
#+nil (defun find-number (x)
	(cond ((numberp x) x)
	      ((atom x) nil)
	      (t (or (find-number (car x))
		     (find-number (cdr x))))))

(defun atoms-to-q (x)
  (cond ((null x) nil)
	((atom x) 'q)
	(t (cons (atoms-to-q (car x))
		 (atoms-to-q (cdr x))))))

;; 8.38
;; removing the first cond clause would have the effect of turning the whole list into 'q
;; edit: I was wrong about this
;; actually, it turns lists into dotted lists instead.

;; 8.39

(defun count-atoms (x)
  "counts the number of atoms in an arbitrarily nested list"
  (cond ((atom x) 1)
	(t (+ (count-atoms (car x))
	      (count-atoms (cdr x))))))

;; 8.40
(defun count-cons (x)
  "Counts the number of cons-cells in an arbitrarily nested list"
  (cond ((atom x) 0)
	(t (+ 1
	      (count-cons (car x))
	      (count-cons (cdr x))))))

;; 8.41
(defun sum-tree (x)
  "Sums all numeric elements of a tree"
  ;; the order of cons clauses is important here.
  (cond ((numberp x) x)
	((atom x) 0)
	(t (+ (sum-tree (car x))
	      (sum-tree (cdr x))))))

;; 8.42
(defun my-subst (old new tree)
  "replaces all instances of `old` in `tree` with `new`"
  (defun worker (x)
    (cond ((equal old x) new)
	  ((atom x) x)
	  (t (cons (worker (car x))
		   (worker (cdr x))))))
  (worker tree))

;; 8.43
(defun flatten (x)
  "returns all elements of an arbitrarily nested list as a single-level list"
  (cond ((null x) nil)
	((atom x) (list x))
	(t (append (flatten (car x))
		   (flatten (cdr x))))))

;; 8.44
(defun tree-depth (tree)
  "Gets the maximum level of nesting of a tree"
  (cond ((atom tree) 0)
	(t (+ 1 (max (tree-depth (car tree))
		     (tree-depth (cdr tree)))))))

;; 8.45
(defun paren-depth (tree)
  "Gets the maximum depth of nested parantheses of a tree"
  ;; basically we only care about when we recur down into the car of a tree.
  ;; I can't tell you how I know this is correct...
  (cond ((atom tree) 0)
	(t (max (+ 1 (paren-depth (car tree)))
		(paren-depth (cdr tree))))))

;; 8.14 -- using helper functions...
;; 8.46
(defun count-up (n)
  (cond ((zerop n) nil)
	(t (append (count-up (- n 1)) (list n)))))

;; 8.47
(defun make-loaf (n)
  "bakes a loaf of size n."
  (if (zerop n) nil
      (cons 'X (make-loaf (- n 1)))))

;; 8.48
;; seems as if I've used single-test augmenting recursion for this one.
(defun bury (x depth)
  "buries x under n levels of parentheses"
  (if (zerop depth) x
      (cons (bury x (- depth 1)) nil)))

;; 8.49
(defun pairings (x y)
  "zips x and y together"
  (if (null x) nil
      (cons (list (car x) (car y))
	    (pairings (cdr x) (cdr y)))))

;; 8.50
(defun sublists (x)
  "returns successive sublists of a list"
  (if (null x) nil
      (cons x (sublists (cdr x)))))

;; 8.51
(defun my-reverse (x)
  "reverses a list"
  (labels ((f (lst reversed)
	     (if (null lst) reversed
		 (f (cdr lst)
		    (cons (car lst) reversed)))))
    (f x '())))

;; 8.52
(defun my-union (x y)
  (cond ((null x) y)
	((null y) x)
	((not (member (car x) y))
	 (cons (car x) (my-union (cdr x) y)))
	(t (my-union (cdr x) y))))

;; 8.53
(defun largest-even (lst)
  "returns the largest even number out of a list"
  (cond ((null lst) 0)
	((evenp (car lst))
	 (max (car lst) (largest-even (cdr lst))))
	(t (largest-even (cdr lst)))))

;; 8.54
(defun huge (n)
  "raises a number to the power equal to itself"
  (labels ((f (number i)
	     (if (zerop i) 1
		 (* number (f number (- i 1))))))
    (f n n)))

;; 8.55
;; recursive functions call themself -- non-recursive ones do not.

;; 8.56
(defun every-other (x)
  "returns 'every other' element of a list"
  (if (null x) nil
      (cons (car x) (every-other (cddr x)))))

;; 8.57
(defun halve (n)
  (ceiling (/ n 2)))

(defun drop (n lst)
  "retrieves all elements of a list after the first 'n' elements"
  (cond ((null lst) nil)
	((zerop n) lst)
	(t (drop (- n 1) (cdr lst)))))

(defun take (n lst)
  "retrieves the first 'n' elements of a list"
  (if (or (null lst) (zerop n)) nil
      (cons (car lst) (take (- n 1) (cdr lst)))))

(defun left-half (lst)
  "retrieves the left-half of a list, rounding up"
  (take (halve (length lst)) lst))

(defun right-half (lst)
  "retrieves the right-half of a list, rounding down"
  (drop (halve (length lst)) lst))

;; 8.58
(defun merge-lists (sorted1 sorted2)
  "merges two sorted lists together in ascending order"
  (cond ((null sorted1) sorted2)
	((null sorted2) sorted1)
	((< (car sorted1) (car sorted2))
	 (cons (car sorted1) (merge-lists (cdr sorted1) sorted2)))
	(t (cons (car sorted2) (merge-lists sorted1 (cdr sorted2))))))

(defun merge-sort (lst)
  "sorts a list by successively sorting sublists, then merging results together"
  (if (= (length lst) 1) lst
      (let ((left (merge-sort (left-half lst)))
	    (right (merge-sort (right-half lst))))
	(merge-lists left right))))

;; 8.59
;; I ran this against my better judgement and had to restart emacs
#+nil
(defun factorial (n)
  "creates an infinite loop"
  (if (zerop n) 1
      (/ (factorial (+ n 1)) (+ n 1))))

;; so, factorial(n) = 1 * 2 * ... * n
;; factorial (n + 1) = 1 * 2 * ... * n * n + 1
;; ((1 * 2 * ... * n ) * (n + 1)) / (n + 1) = 1 * 2 * ... * n
;;   the (n + 1) term factors out because of division
;; so, this version of factorial doesn't know when to stop.

;; 8.60

;; (name father mother)
(defparameter family
  '((colin nil nil)
    (deirdre nil nil)
    (arther nil nil)
    (kate nil nil)
    (frank nil nil)
    (linda nil nil)
    (suzanne colin deirdre)
    (bruce arthur kate)
    (charles arthur kate)
    (david arthur kate)
    (ellen arthur kate)
    (george frank linda)
    (hillary frank linda)
    (andre nil nil)
    (tamara bruce suzanne)
    (vincent bruce suzanne)
    (wanda nil nil)
    (ivan george ellen)
    (julie george ellen)
    (marie george ellen)
    (nigel andre hillary)
    (frederick nil tamara)
    (zelda vincent wanda)
    (joshua ivan wanda)
    (quentin nil nil)
    (robert quentin julie)
    (olivia nigel marie)
    (peter nigel marie)
    (erica nil nil)
    (yvette robert zelda)
    (diane peter erica)))

(defun get-record (name)
  "retrieves a person's record out of the family database"
  (assoc name family))

(defun father (person)
  "retrieves the father of a person"
  (if (null person) nil
      (cadr (get-record person))))

(defun mother (person)
  "retrieves the mother of a person"
  (if (null person) nil
      (caddr (get-record person))))

(defun parents (person)
  "retrieves the parents of a person in (father mother) form"
  (if (null person) nil
      (cdr (get-record person))))

(defun parent-of (parent person)
  (member parent (parents person)))

;; implementing this with recursion because I am trying to be a good sport
(defun children (person)
  "retrieves the children of a person"
  (labels ((is-parent-of (child-record)
	     "determines if the given person is in one of the two parent slots"
	     (member person (cdr child-record)))
	   (f (children library)
	     "accumulates a list of the given person's children out of the library"
	     (cond ((null library) children)
		   ((is-parent-of (car library))
		    (f (cons (caar library) children) (cdr library)))
		   (t (f children (cdr library))))))
    (if (null person) nil
	(f nil family))))

;; 8.60.b
(defun siblings (person)
  "returns the set of all siblings of a person, minus that person, including half-siblings"
  (if (null person) nil
      (let ((papa (father person))
	    (mummy (mother person)))
	(remove person (union (children papa)
			      (children mummy))))))

;; 8.60.c
(defun mapunion (f lst)
  "applies f to every element of lst, and computes the union across all resulting sets"
  ;; (reduce #'union (mapcar f lst))
  ;; the book defines it thus:
  (and lst (reduce #'union (mapcar f lst))))


;; 8.60.d
(defun grandparents (person)
  "retrieves the set of a person's grandparents -- that is, their parent's parents"
  (if (null person) nil
      (mapunion #'parents (parents person))))

;; 8.60.e
(defun cousins (person)
  "retrieves the set of a person's cousins -- the children of their parent's siblings"
  (if (null person) nil
      (mapunion #'children
		(mapunion #'siblings
			  (parents person)))))

;; 8.60.f
(defun descended-from (descendent ancestor)
  "Predicate determining whether person 1 (descendent) is descended from person 2 (ancestor)"
  (let ((par (parents descendent)))
    (cond ((null par) nil)
	  ((member ancestor par) t)
	  (t (or (descended-from (car par) ancestor)
		 (descended-from (cadr par) ancestor))))))

;; 8.60.g
(defun ancestors (person)
  "returns the set of a person's ancestors -- that is, their parents, and their parents parents, and so on."
  ;; extract the non-nil parents from the person
  (let ((known-parents (remove-if #'null (parents person))))
    (if (null known-parents) nil
	(union known-parents (mapunion #'ancestors known-parents)))))


;; 8.60.h
(defun min-or-not-nil (x y)
  "either returns the minimum value between x and y, or the non-nil value if one is nil, or nil if both are nil"
  (if (and x y) (min x y)
      (or x y)))

(defun increment-or-nil (n)
  "increments the value of n if it is not nil, or simply passes through nil if it is"
  (unless (null n) (+ 1 n)))

(defun generation-gap (descendent ancestor)
  "returns the number of generations between a person and their ancestor, or nil if they are not lineated"
  (let ((known-parents (remove-if #'null (parents descendent))))
    ;; first case, where the descendent has no known parents.
    (cond ((null known-parents) nil)
	  ;; second case, where ancestors is one of the descendent's known parents
	  ((member ancestor known-parents) 1)
	  (t (increment-or-nil
	      (reduce #'min-or-not-nil
		      (mapcar (lambda (p) (generation-gap p ancestor))
			      known-parents)))))))

;; 8.60.i
;; 1 - is robert descended from deirdre?
(descended-from 'robert 'deirdre)
;; NIL
;; 2 - who are yvette's ancestors?
(ancestors 'yvette)
;; (WANDA VINCENT SUZANNE BRUCE ARTHUR KATE DEIRDRE COLIN LINDA FRANK GEORGE ELLEN
;;  QUENTIN JULIE ROBERT ZELDA)
;; 3 - what is the generation gap between olivia and frank?
(generation-gap 'olivia 'frank)
;; 3
;; 4 - who are peter's cousins?
(cousins 'peter)
;; (JOSHUA ROBERT)
;; 5 - who are olivia's grandparents?
(grandparents 'olivia)
;; (HILLARY ANDRE GEORGE ELLEN)

;; 8.61
(defun count-up-helper (curr acc)
  "tail-recursive helper function for tr-count-up. broken out for tracing."
  (if (zerop curr) acc
      (count-up-helper (- curr 1) (cons curr acc))))

(defun tr-count-up (n)
  (count-up-helper n nil))

;; 8.62
(defun fact-helper (curr acc)
  (if (zerop curr) acc
      (fact-helper (- curr 1) (* curr acc))))

(defun tr-fact (n)
  (fact-helper n 1))

;; 8.63
(defun tr-union-helper (currx curry acc)
  ;; first case, where currx is nil. Simply append curry onto acc and return.
  (cond ((null currx) (append curry acc))
	;; second case, where curry is nil. appand curry onto acc and return.
	((null curry) (append currx acc))
	;; third case, where (car currx) is not in curry. add it to acc and recur on the cdr of currx
	((not (member (car currx) curry))
	 (tr-union-helper (cdr currx) curry (cons (car currx) acc)))
	;; third case, where (car currx) is in curry. do not add it to acc, and recur on the cdr of currx.
	;; note that we never modify curry. that is because when we have exhausted currx, we append curry onto acc.
	(t (tr-union-helper (cdr currx) curry acc))))

(defun tr-union (x y)
  (tr-union-helper x y nil))

(defun tr-set-difference-helper (set1 set2 acc)
  ;; first case, set1 is nil, so return nil
  (cond ((null set1) acc)
	;; second case, (car set1) is not a member of set2, so include it in the difference
	((not (member (car set1) set2))
	 (tr-set-difference-helper (cdr set1) set2 (cons (car set1) acc)))
	;; third case, (car set1) is a member of set2, so don't include it in the difference
	(t (tr-set-difference-helper (cdr set1) set2 acc))))

(defun tr-set-difference (x y)
  (if (null y) x
      (tr-set-difference-helper x y nil)))

(defun tr-intersection-helper (set1 set2 acc)
  ;; first case, set1 is nil, so return acc
  (cond ((null set1) acc)
	;; second case, (aar set1) is a member of set2, so include it in the intersection
	((member (car set1) set2)
	 (tr-intersection-helper (cdr set1) set2 (cons (car set1) acc)))
	;; third case, (car set1) is not a member of set2, so ignore it in the intersection
	(t (tr-intersection-helper (cdr set1) set2 acc))))

(defun tr-intersection (x y)
  (if (null y) nil
      (tr-intersection-helper x y nil)))

;; (defun my-union (x y)
;;   (cond ((null x) y)
;; 	((null y) x)
;; 	((not (member (car x) y))
;; 	 (cons (car x) (my-union (cdr x) y)))
;; 	(t (my-union (cdr x) y))))

;; 8.64
;;; this is kind of odd. 
;;; I'm going to do a thing with exception handling here
(defun tree-find-if (pred tree)
  ;; we have to catch type-errors for our unary predicate
  ;; otherwise we will get errors when we try and use it.
  ;; I wonder if there's a better way to do this
	((atom tree) nil)
	(t (or (tree-find-if pred (car tree))
	       (tree-find-if pred (cdr tree))))))

;; I'm not sure why but this function destroyed my poor editor's indenting.
;; 8.65
;; (defun tr-slices (slices)
;; ;;   "it's a list-length function"
;; ;;   (labels ((fn (lst counter)
;; ;; 	   (if (null lst) counter
;; ;; 	       (fn (cdr lst) (+ 1 counter)))))
;; ;;   (fn slices 0)))
;; 
;; (defun tr-reverse (lst)
;; ;;   (labels ((fn (x acc)
;; ;; 	   (if (null x) acc
;; ;; 	       (fn (cdr x) (cons (car x) acc)))))
;; ;;   (fn lst nil)))


;; recursive data-structures
;; an S-expression is either
;;  - an atom
;;  - or a cons cell whose CAR and CDR parts are S-expressions

;; 8.66 - arith-eval, a function that evaluates arithmetic expressions.

(defun arith-eval (expr)
  "a function that evaluates arithmetic expressions"
  (if (numberp expr) expr
      (funcall (cadr expr)
	       (arith-eval (car expr))
	       (arith-eval (caddr expr)))))

;; 8.67
(defun legalp (expr)
  "a function that determines if the given expression is a valid arithmetic expression"
  (labels ((operatorp (sym)
	     (member sym '(+ - * /))))
    ;; either the expr is a number
    (or (numberp expr)
	;; OR, it is NOT an atom
	    (and (not (atom expr))
		 ;; AND it is composed of two legal expressions surrounding an operator.
		 (and
		  (legalp (car expr))
		  (operatorp (cadr expr))
		  (legalp (caddr expr)))))))

;; 8.68
;; NIL is a proper list and so is any cons cell whose cdr component is a proper list.
(defun proper-listp (lst)
  "determines if lst is a proper list"
  (or (null lst)
    (proper-listp (cdr lst))))

;; 8.69
;; a positive integer greater than 1 is either
;;  a prime number or
;;  the product of 2 or more prime numbers (incomplete. answer, see below)
;;  the product of a prime number and another positive integer greater than one

;; 8.70
(defun factors (n)
  (factors-help n 2))

(defun factors-help (n p)
  ;; first case, where n is one. 1's factors are, of course, 1
  (cond ((equal n 1) nil)
	;; second case, where p divides n.
	((zerop (rem n p))
	 ;; add p onto the list of factors, and recur on (/ n p), p
	 (cons p (factors-help (/ n p) p)))
	;; p does not divide n -- so recur on n and (+ p 1)
	(t (factors-help n (+ p 1)))))

;; 8.70
(defun factor-tree (n)
  (factor-tree-help n 2))

(defun factor-tree-help (n p)
  (cond ((equal n 1) nil)
	((equal n p) n)
	((zerop (rem n p))
	 (list n p (factor-tree-help (/ n p) p)))
	(t (factor-tree-help n (+ p 1)))))


;; 8.71
;; (A B (C D) E)
;;       .
;;      / \
;;     /  /\
;;    /  / /\
;;   /  / /  \
;;  /  / / \  \
;; A  B  C  D  E

;; 8.72
;;  veins have arbitrary amounts of branching
;; a trie representing words in the 26-letter english alphabet are 26-trees
