
;;;; double test tail recursion
#+nil
(defun func (x)
  (cond (end-test-1 end-value-1)
	(end-test-2 end-value-2)
	(T (func reduced-x))))

;; example: anyoddp
(defun anyoddp (x)
  (cond ((null x) nil)
	((oddp (car x)) t)
	(t (anyoddp (cdr x)))))

;;;; single-test tail recursion
#+nil
(defun func (x)
  (if end-test end-value
      (func reduced-x)))

;; example: find-first-atom
(defun find-first-atom (x)
  (if (atom x) x
      (find-first-atom (car x))))

;;;; single-test augmenting recursion
#+nil
(defun func (x)
  (if end-test end-value
      (aug-fun aug-val
	       (func reduced-x))))

;; example:
(defun count-slices (x)
  (if (null x) 0
      (+ 1 (count-slices (cdr x)))))

;;; list-consing recursion, a special case of augmenting recursion
#+nil
(defun func (x)
  (if end-test nil
      (cons new-element
	    (func reduced-x))))
;; so, aug-fun is cons
;;     aug-val is a new list element
;;     end-value is nil

;; example:
(defun laugh (n)
  (if (zerop n) nil
      (cons 'ha (laugh (- n 1)))))

;;;; simultaneous recursion on several variables
#+nil
(defun func (n x)
  (if end-test end-value
      (func reduced-n reduced-x)))

(defun my-nth (n x)
  (if (zerop n) (car x)
      (my-nth (- n 1) (cdr x))))

;; and of course, we could do double-test simultaneous recursion on multiple variables as well

;;;; conditional augmentation
#+nil
(defun func (x)
  (cond (end-test end-value)
	(aug-test (aug-fun aug-val
			   (func reduced-x)))
	(t (func reduced-x))))

;; example: extract symbols
;; this is also an example of conditional list-consing recursion
(defun extract-symbols (x)
  (cond ((null x) nil)
	((symbolp (car x))
	 (cons (car x)
	       (extract-symbols (cdr x))))
	(t (extract-symbols (cdr x)))))

;;;; multiple recursion
#+nil
(defun func (n)
  (cond (end-test-1 end-value-1)
	(end-test-2 end-value-2)
	(t (combiner (func first-reduced n)
		     (func second-reduced-n)))))

;; example: fib
(defun fib (n)
  (cond ((zerop n) 1)
	((= n 1) 1)
	(t (+ (fib (- n 1))
	      (fib (- n 2))))))

;;; car/cdr recursion, a special case of multiple recursion
#+nil
(defun func (x)
  (cond (end-test-1 end-value-1)
	(end-test-2 end-value-2)
	(t (combiner (func (car x))
		     (func (cdr x))))))

(defun find-number (x)
  (cond ((numberp x) x)
	((atom x) nil)
	(t (or (find-number (car x))
	       (find-number (cdr x))))))
