(defvar lst '(a b c d e f g))
;;; 6.6
#+nil
(defun last-element (x)
  (car (last x)))

#+nil
(defun last-element (x)
  (car (reverse x)))

(defun last-element (x)
  (when x
    (nth (- (length x) 1) x)))

;;; 6.7
#+nil
(defun next-to-last (x)
  (cadr (reverse x)))

(next-to-last lst)
;; F

(defun next-to-last (x)
  (when x
    (nth (- (length x) 2) x)))

(next-to-last lst)
;; F

;;; 6.8
(defun my-butlast (x)
  (reverse (cdr (reverse x))))

(my-butlast lst)

;; 6.9
(defun mystery (x) (first last (reverse x))) ; this is the same as car

;; 6.10
(defun palindromep (x)
  (equal x (reverse x)))

;; 6.11
(defun make-palindrome (x)
  (append x (reverse x)))

;; 6.26
(defun right-side (features)
  "returns all features in a list to the right of the -VS- symbol"
  (cdr (member '-VS- features)))

(defun left-side (features)
  "returns all features in a list to the left of the -VS- symbol"
  (cdr (member '-VS- (reverse features))))

(defun count-common (features)
  "returns the number of common elements on the right and left side of a feature list"
  (length (intersection (left-side features) (right-side features))))

(defun compare (features)
  "reports the number of common elements on the right and left size of a feature list"
  `(,(count-common features) common features))

;; 6.30

(defvar books
  '((small-gods                  . terry-pratchett)
    (naked-lunch                 . william-burroughs)
    (the-unique-and-its-property . max-stirner)
    (anti-oedipus                . d+g)
    (a-thousand-plateus          . d+g)
    (the-light-fantastic         . terry-pratchett)))

;; 6.31
(defun who-wrote (book)
  (cdr (assoc book books)))

;; 6.33
;;; in order to do this with assoc instead of rassoc, the table entries would need to be (author book) rather than (book author)
(defun wrote-what (author)
  (car (rassoc author books)))


;;; 6.34
(defvar atlas
  '((pennsylvania pittsburgh johnstown)
    (new-jersey newark princeton trenton)
    (ohio columbus)))


;;; 6.35
(defvar nerd-states
  '((sleeping eating)
    (eating waiting-for-a-computer)
    (waiting-for-a-computer programming)
    (programming debugging)
    (debugging sleeping)))

(defun nerdus (state)
  "looks up the next state for the north-american nerd"
  (cadr (assoc state nerd-states)))

(nerdus 'playing-guitar)
;; NIL

(defun sleepless-nerd (state)
  "looks up the next state for a nerd that's had a little too much coffee"
  (let ((next (nerdus state)))
    (if (equal next 'sleeping)
	(nerdus next)
	next)))

(defun nerd-on-caffeine (state)
  (nerdus (nerdus state)))

;; 3 state changes required to go from programming to debugging for the poor nerd on caffeine
(nerd-on-caffeine
 (nerd-on-caffeine
  (nerd-on-caffeine 'programming)))
;; DEBUGGING

;; 6.36
(defun swap-first-last (lst)
  "swaps the first and last elements of a list."
  (if (> (length lst) 1)
      (append (last lst)
	      (butlast (cdr lst))
	      (list (car lst)))
      lst))

;; 6.37
(defun rotate-left (lst)
  "rotates the list one element to the left (first element becomes last)"
  (append (cdr lst) (list (car lst))))

(defun rotate-right (lst)
  "rotates the list one element to the right (last element becomes first)"
  (append (last lst) (butlast lst)))

(defvar rooms
      '((living-room        (north front-stairs)
			    (south dining-room)
			    (east kitchen))

	(upstairs-bedroom   (west library)
			    (south front-stairs))

	(dining-room        (north living-room)
			    (east pantry)
			    (west downstairs-bedroom))

	(kitchen            (west living-room)
			    (south pantry))

	(pantry             (north kitchen)
			    (west dining-room))

	(downstairs-bedroom (north back-stairs)
			    (east dining-room))

	(back-stairs        (south downstairs-bedroom)
			    (north library))

	(front-stairs       (north upstairs-bedroom)
			    (south living-room))

	(library            (east upstairs-bedroom)
			    (south back-stairs))))

(defvar loc nil)

(defun choices (from-room)
  "Gets the possible directions,rooms available from FROM-ROOM"
  (cdr (assoc from-room rooms)))

(defun look (direction room)
  "looks up the room that moving in DIRECTION from ROOM leads to"
  (cadr (assoc direction (choices room))))

(defun set-robbie-location (place)
  "moves robbie to PLACE by setting the variable LOC."
  (setf loc place))

(defun how-many-choices ()
  "Determines the number of directions robbie can go from LOC"
  (length (choices loc)))

(defun upstairsp (room)
  "checks if robbie is in the library or the upstairs-bedroom"
  (or (equal room 'library)
      (equal room 'upstairs-bedroom)))

(defun onstairsp (room)
  "Checks if robbie is on front-stairs or back-stairs"
  (or (equal room 'front-stairs)
      (equal room 'back-stairs)))

(defun whereis ()
  "Displays robbie's currrently location, with flavor text" 
  `(robbie is
	   ,@(cond ((onstairsp loc) `(on the ,loc))
		   ((upstairsp loc) `(upstairs in the ,loc))
		   (t `(downstairs in the ,loc)))))

(defun move (direction)
  "attempts to move robbie to the room in DIRECTION.
If successful, update robbies location and print where he is
otherwise, notify that we failed"
  (let ((room (look direction loc)))
    (if room
	(progn (set-robbie-location room)
	       (whereis))
	'(Ouch! robbie hit a wall))))

