(defun new-histogram (size)
  "creates a new histogram (a zeroed out array) of a given size"
  (make-array size :initial-element 0))


(defun record-value (v hist)
  "increments the counter for the given value for our histogram"
  (incf (aref hist v)))

(defun print-histogram (hist)
  )

(defparameter *hist-size* 40)
(defparameter *hist-array* (new-histogram *hist-size*))

(defun repeat-string (s times )
  (with-output-to-string (str)
      (dotimes (_ times str)
	(format str "~a" s))))

(defun print-hist-line (histogram line)
  (let ((v (aref histogram line)))
    (format t "~&~2d [~3d] ~a"
	    line v
	    (repeat-string "*" v))))

(defun print-histogram (histogram &aux (len (length histogram)))
  (dotimes (i len)
    (print-hist-line histogram i)))





