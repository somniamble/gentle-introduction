;; running this after doing keyboard exercise 2
(room)
;; Dynamic space usage is:   65,580,256 bytes.
;; Immobile space usage is:  14,765,216 bytes (35,456 bytes overhead).
;; Read-only space usage is:          0 bytes.
;; Static space usage is:           736 bytes.
;; Control stack usage is:        3,648 bytes.
;; Binding stack usage is:          640 bytes.
;; Control and binding stack usage is for the current thread only.
;; Garbage collection is currently enabled.
;; 
;; Breakdown for dynamic space:
  ;; 17,856,912 bytes for   241,994 instance objects
  ;; 13,145,136 bytes for   821,571 cons objects
  ;; 12,136,656 bytes for    82,869 simple-vector objects
   ;; 4,560,592 bytes for    10,330 simple-character-string objects
  ;; 14,845,104 bytes for   322,490 other objects
;; 
  ;; 62,544,400 bytes for 1,479,254 dynamic objects (space total)
;; 
;; Breakdown for immobile space:
  ;; 12,692,640 bytes for 20,318 code objects
   ;; 1,244,608 bytes for 25,916 symbol objects
     ;; 792,512 bytes for 21,536 other objects
;; 
  ;; 14,729,760 bytes for 67,770 immobile objects (space total)
;; 
;; 
;; 
