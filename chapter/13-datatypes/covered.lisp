;; array functions
MAKE-ARRAY, AREF

;; Printer Switch
*PRINT-ARRAY*

;; HASH table function
MAKE-HASH-TABLE, GETHASH

;; property list functions
GET, SYMBOL-PLIST, REMPROP
