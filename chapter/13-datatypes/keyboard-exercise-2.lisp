(defparameter crypto-text
  '("zj ze kljjls jf slapzi ezvlij pib kl jufwxuj p hffv jupi jf"
    "enlpo pib slafml pvv bfwkj"))

;; a
(defparameter *encipher-table* (make-hash-table))
(defparameter *decipher-table* (make-hash-table))

;; b
(defun make-substitution (char1 char2)
  "char1 deciphers to char2, char2 enciphers to char1"
  (setf (gethash char1 *decipher-table*) char2)
  (setf (gethash char2 *encipher-table*) char1))

;; c
(defun undo-substitution (char)
  "sets the entry of char in *decipher-table* to nil and the entry of the char it deciphers to to nil"
  (let ((encchar (gethash char *decipher-table*)))
    (setf (gethash char *decipher-table*) nil)
    (when encchar
      (setf (gethash encchar *encipher-table*) nil))))


;; d
(defun clear ()
  (clrhash *encipher-table*)
  (clrhash *decipher-table*))

(defun decipher (char)
  "gets the character the given char deciphers to or nil"
  (gethash char *decipher-table*))

(defun encipher (char)
  "gets the character the given char enciphers to"
  (gethash char *encipher-table*))

;; e
(defun decipher-string (string)
  (do* ((len (length string))
	(s (make-string len :initial-element #\space))
	(index 0 (1+ index)))
       ((= index len) s)
    (let* ((curchar (aref string index))
	  (curdeciphered (decipher curchar)))
      (when curdeciphered
	(setf (aref s index) curdeciphered)))))

;; f
(defun format-line (string)
  (let ((deciphered (decipher-string string)))
    (format nil "~&~a~&~a" string deciphered)))

;; g
(defun show-text (cryptogram)
  (format t "~&--------------------~{~2&~a~}~&--------------------"
	  (mapcar #'format-line cryptogram)))

;; h
(defun get-first-char (x)
  (char-downcase
   (char (format nil "~A" x) 0)))

(defun read-letter ()
  "reads in some input passing through END or UNDO otherwise getting the first char"
  (let ((object (read)))
    (if (member object '(END UNDO CLEAR))
	object
	(get-first-char object))))

(defun not-a-letter (obj)
  (not (and (characterp obj) (alpha-char-p obj))))

(defun sub-letter (char)
  (let ((dec (decipher char)))
    (if dec
	(format t "~&~a already deciphers to ~a" char dec)
	(progn (format t "~&what does ~a decipher to?~&> " char)
	       (let* ((subchar (read-letter))
		      (enc (encipher subchar)))
		 (cond ((not-a-letter subchar)
			(format t "~&~A is not a letter" subchar))
		       (enc
			(format t "~&~A already enciphers to ~A" subchar enc))
		       (t (make-substitution char subchar)
			  (list char subchar))))))))

(defun undo-letter ()
  (let ((char (read-letter)))
    ;; first case where char is not a letter
    (cond ((not-a-letter char)
	   (format t "~&~a is not a letter" char))
	  ;; second case where char ciphers to something
	  ((decipher char) (undo-substitution char))
	  ;; third case where char does not cipher to anything
	  (t (format t "~&~a does not cipher to anything" char)))))

(defun solve (cryptogram)
  (show-text cryptogram)
  (format t "~&Substitute which letter?~&> ")
  (do ((input (read-letter) (read-letter)))
      ((eq input 'END) t)
    (cond ((eq 'undo input) (undo-letter))
	  ((eq 'clear input) (clear) (format t "~&CLEARING ALL"))
	  ((not-a-letter input) (format t "~&~a is not a letter" input))
	  (t (sub-letter input)))
    (show-text cryptogram)
    (format t "~&Substitute which letter?~&> ")))

;; bonus
(defun print-cipher (cipher)
  (loop for k being each hash-key of cipher
	  using (hash-value v)
	do (format t "~&~a => ~a" k v))))
(print-cipher *decipher-table*)
;; 
;; p => a
;; z => i
;; v => l
;; f => o
;; h => f
;; j => t
;; e => s
;; i => n
;; l => e
;; k => b
;; s => r
;; a => m
;; b => d
;; u => h
;; w => u
;; x => g
;; m => v
;; n => p
;; o => k
;; 

(show-text crypto-text)
;; --------------------
;; 
;; zj ze kljjls jf slapzi ezvlij pib kl jufwxuj p hffv jupi jf
;; it is better to remain silent and be thought a fool than to
;; 
;; enlpo pib slafml pvv bfwkj
;; speak and remove all doubt
;; --------------------
;; 
;; NIL
