;; make an array
'#(tuning violin a 440)

(defparameter foo #(nil nil nil nil nil))
(setf (aref foo 0) 'fuck)
(setf (aref foo 4) 'ohno)
(eval foo)
#(FUCK NIL NIL NIL OHNO)

;; creating with make-array
;; lets you do some extra nice stuff like
;; specify an initial element
(make-array 5 :initial-element 'eye)
#(EYE EYE EYE EYE EYE)

;; or specify an amount of initial elements
(make-array 5 :initial-contents '(to live to die to))
#(TO LIVE TO DIE TO)

(make-array '(4 4) :initial-element 0)
#2A((0 0 0 0) (0 0 0 0) (0 0 0 0) (0 0 0 0))

(make-array '(3 3 3) :initial-element " ")
#3A(((" " " " " ") (" " " " " ") (" " " " " "))
    ((" " " " " ") (" " " " " ") (" " " " " "))
    ((" " " " " ") (" " " " " ") (" " " " " ")))

;; also things that work on sequences work on arrays for free
;; my wrists hurt so m not doing examples
;; e.f. length, reverse, find-if
;; mapcar DOESNT but MAP DOES
;;  map is more generic

(aref "conk" 2)
#\n
(type-of (aref "conk" 2))
STANDARD-CHAR

(setf (aref "conk" 2) #\c)
#\c

;; hash tables 8*)

(defparameter hhh (make-hash-table))
HHH

(setf (gethash 100 hhh) '("test"))
("test")
;; nice
(push "the legend of dickneck" (gethash 100 hhh))
("the legend of dickneck"
 "test")
(push "checking my mentions" (gethash 100 hhh))
("checking my mentions"
 "the legend of dickneck"
 "test")
(pushnew "test" (gethash 100 hhh))
("test"
 "checking my mentions"
 "the legend of dickneck"
 "test")

(describe hhh)
;; #<HASH-TABLE :TEST EQL :COUNT 1 {10034328F3}>
  ;; [hash-table]
;; 
;; Occupancy: 0.1
;; Rehash-threshold: 1.0
;; Rehash-size: 1.5
;; Size: 7
;; Synchronized: no

;; (inspect hhh)
(push "the sky and the cosmos are one" (gethash 498 hhh))
;; ("the sky and the cosmos are one")

(push "corpsefucker trannies" (gethash 420 hhh))
;; ("corpsefucker trannies")

;; oh boy here's some ancient shit you won't care about its PROPERTY LISTS
;; property lists are like this: (indicator-one value-one indicator-two value-two ...)
(setf (get 'fred 'sex) 'male)
(setf (get 'fred 'age) 23)
(setf (get 'fred 'siblings) '(george wanda))
(describe 'fred)
;; COMMON-LISP-USER::FRED
  ;; [symbol]
;; 
;; Symbol-plist:
  ;; SIBLINGS -> (GEORGE WANDA)
  ;; AGE -> 23
  ;; SEX -> MALE
(symbol-plist 'fred)
;; (SIBLINGS
;;  (GEORGE WANDA)
;;  AGE
;;  23
;;  SEX
;;  MALE)

;; here's something interesting
;; pushnew uses eq by default?
(defun addprop (sym elem prop)
  (pushnew elem (get sym prop)))


(defun record-meeting (x y)
  (addprop x y 'has-met)
  (addprop y x 'has-met)
  t)

;; 13.1
(defun subprop (sym elem prop)
  (setf (get sym prop)
 	(delete elem (get sym prop))))

;; 13.2
(defun forget-meeting (x y)
  (subprop x y 'has-met)
  (subprop y x 'has-met)
  t)

;; 13.3
(defun my-get (sym property)
  (cadr (member property (symbol-plist sym))))

;; 13.4
(defun hasprop (sym property)
  (when (member property (symbol-plist sym))
    t))

;; 13.5
;; arrays have constant random-access time, whereas lists have O(n) access time
;;   (assuming you're not popping off the very front

;; 13.6
;; lists are easier to dynamically allocate, since each memory cell needn't be adjacent to the next
;; array resizing is much more expensive, however, since you have to allocate a new bigger block of memory and copy everything over

;; here's what an alist of dotted pairs looks like....
;;
;; 8 cons cells
;; #:# -> #:# -> #:# -> #:# -> NIL
;; |      |      |      |
;; A:0    B:2    C:4    D:8

;; an equivalent plist

;; #:# -> #:# -> #:# -> #:# -> #:# -> #:# -> #:# -> #:# -> NIL
;; |      |      |      |      |      |      |      |
;; A      0      B      2      C      4      D      8
;; what do you know! it's the same.
;; it's just a matter of nesting
