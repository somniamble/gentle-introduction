;; FORMATTING BASICS
;; 9.1
(defun pilots ()
  (format t "There are old pilots,~%and there are bold pilots,~%but there are no old bold pilots."))


;; 9.2
(defun draw-line (n)
  "draws a line of n asterisks"
  (if (zerop n) nil
      (progn (format t "*")
	     (draw-line (- n 1)))))

;; 9.3
(defun draw-box (length lines)
  "draws a box a certain number of asterisks long and lines high"
  (if (zerop lines) nil
      (progn (draw-line length)
	     (fresh-line)
	     (draw-box length (- lines 1)))))

;; 9.4
(defun ninety-nine-bottles (n)
  "sings the classic tune"
  (if (zerop n)
      (format t "no more bottles of beer.... the reckoning is at hand~%")
      (progn (format t "~a bottles of beer on the wall,
~a bottles of beer
Take one down,
Pass it around,
~a bottles of beer on the wall.~%~%" n n (- n 1))
	     (ninety-nine-bottles (- n 1)))))

;; 9.5
(defun print-board (state)
  "prints a tic-tac-toe board"
  (let ((printable
	  
	  (mapcar (lambda (tick)
		    ;; turn the nil entries into spaces, for printing
		    (if (null tick) #\space
			tick))
		  state)))
    (format t "~{~& ~a | ~a | ~a ~&~^-----------~}" printable)))

;;; READING INPUT BASICS
;; read function - reads in a single value
;; a number, symbol, list, or string (generally)
;; (read) "this is a test"
;;  -> "this is a test"
;; (read) this is a test
;;  -> THIS
;; 9.6
(defun prompt (prompt-text &optional (read-fn #'read))
  (format t "~&~a~&> " prompt-text)
  (funcall read-fn))

;; (defun wagie-cagie ()
;;   (let ((rate (progn
;; 		(format t "~&what is your hourly pay~&> ")
;; 		(read)))
;; 	(hours (progn
;; 		 (format t "~&how many hours did you work?~&> ")
;; 		 (read))))
;;     (format t "Your gross pay is $~$..." (* rate hours))))

;; 9.6
(defun wagie-cagie ()
  (let ((rate (prompt "What is your hourly pay"))
	(hours (prompt "how many hours have you worked")))
    (format t "Your gross pay is $~$..." (* rate hours))))

;; 9.7
(defun cookie-monster ()
  (let ((input (prompt "please sir cookie sir?")))
    (if (equal input 'cookie) (format t "~&thank you for the cukie :)")
	(progn (format t "~&I don't want a ~a..." input)
	       (cookie-monster)))))

;; important thing to note -- if you call READ, leave input
;;   in the buffer, and call it again, then READ will consume the next item
;;   that it can read out of the input. for example:
;;   (progn (read) (read)) > hello there
;;     => THERE ; prompts for input once 


(defun riddle ()
  (if (yes-or-no-p "do you seek enlightenment? ")
      (format t "then do not ask for it")
      (format t "you have found it")))
;; do you seek enlightenment?  (yes or no) yes
;; then do not ask for it

;; do you seek enlightenment?  (yes or no) no
;; you have found it

;; do you seek enlightenment?  (yes or no) banana
;; 
;; Please type "yes" for yes or "no" for no.
;; do you seek enlightenment?  (yes or no) 

(defun do-you-wish-to-continue ()
  (if (y-or-n-p "do you wish to continue")
      (format t "onto the next")
      (format t "mods arrest this man")))
;; same as above

;;; READING FROM FILES

;; (with-open-file (streamname pathname)
;;    ...body)
;; streamname is a variable that can be referenced in body
;; pathname is just the path to the file

;;; WRITING TO FILES
;; (with-open-file (streamname pathname :direction :output)
;;   ...body)
;; if you write out to a stream using ~S then you can be sure of being able
;;   to write back to it...
;;  e.g. (format streamname "~&~S~&" '(easy as a b c 1 2 3))

;; 9.8
;; Strings differ from symbols in some of the following ways:
;;   strings are sequences of characters, meaning they can be
;;   broken down and treated as lists (in fact they are a subtype of vector)
;; Strings are denoted by arbitrary characters within enclosing double-quotes
;; Strings preserve case, formatting, etc... because they are character-vectors,
;;   and not a symbol name

;; 9.9
;; (format t "a~S" 'b)
;; aB

;; (format t "always~%broke")
;; always
;; broke

;; (format t "~S~S" 'alpha 'bet)
;; ALPHABET

;;; some stream names
;; https://www.cs.cmu.edu/Groups/AI/html/cltl/clm/node183.html
;; *standard-output* -- the stream corresponding to the console output (default)
;; *standard-input* -- the stream corresponding to console input (default)
;; *error-output* -- another console output stream, but for error messages
;; *query-io* -- the stream used for writing questions out to the user, and
;;                 retrieving the answers to questions from the user.
;; -- When the normal input to a program may be coming from a file, questions such as ``Do you really want to delete all of the files in your directory?'' should nevertheless be sent directly to the user; and the answer should come from the user, not from the data file. For such purposes *query-io* should be used instead of *standard-input* and *standard-output*. *query-io* is used by such functions as yes-or-no-p. 
;; *terminal-io* -- The value of *terminal-io* is ordinarily the stream that
;;   connects to the user's console. Typically, writing to this stream would
;;   cause the output to appear on a display screen, for example, and reading
;;   from the stream would accept input from a keyboard.
;; --  It is intended that standard input functions such as read and read-char,
;;     when used with this stream, would cause ``echoing'' of the input into the
;;     output side of the stream. (The means by which this is accomplished are
				;; of course highly implementation-dependent.) 


;; 9.10
;; KEYBOARD EXERCISE
;; this doesn't work in slime, but does work on the command line.
;;   very curious
;; (defun progress-bar ()
;;   (dotimes (i 100)
;;     (format t (format nil "~~1,1,~d,'=:<>~~>" i))
;;     (finish-output)
;;     (sleep 1)
;;     (dotimes (j (1+ i))
;;       (write-char #\Backspace))))
;; (progress-bar)

;; 9.10.a
(defun space-over (n)
  "prints n spaces"
  (cond ((zerop n) nil)
	((< n 0) (princ "Error!"))
	(t (princ #\space)
	   (space-over (- n 1)))))

(defun test (n)
  (format t "~%>>>")
  (space-over n)
  (format t "<<<"))

;; 9.10.b
(defun plot-one-point (plotting-string y-val)
  "prints plotting-string at column y-val, then prints a new line"
  (space-over y-val)
  (format t "~a~%" plotting-string))

;; 9.10.c
(defun plot-points (plotting-string y-vals)
  "prints plotting-string on multiple lines according to the positions in y-vals"
  (mapcar (lambda (y) (plot-one-point plotting-string y)) y-vals))

;; 9.10.d
(defun range (m n)
  "generates the list of numbers in the inclusive interval m -> n"
  (cond ((= m n) (list m))
	;; m < n, ascending range
	((< m n) (cons m (range (+ m 1) n)))
	;; m > n, descending range 
	((> m n) (cons m (range (- m 1) n)))))

;; 9.10.e
(defun make-graph (func start end plotting-string)
  "creates a simple graph of a function's outputs over a given domain"
  (let* ((rng (range start end))
	 (values (mapcar func rng)))
    (fresh-line)
    (plot-points plotting-string values)))


;; 9.10.f
(defun square (n) (* n n))

(make-graph #'square -7 7 "sleeper")

;; 9.11
(defun dot-prin1 (lst)
  "takes a list (proper or improper) and prints it in dotted list format"
  (cond ((atom lst) (format t "~S" lst))
	(t (format t "(")
	   (dot-prin1 (car lst))
	   (format t " . ")
	   (dot-prin1 (cdr lst))
	   (format t ")"))))

;; 9.12
;; (dot-prin1 '(a . (b . (c))))
;; (A . (B . (C . NIL)))

;; 9.13
;; hypothesis: (read '(a . b)) evaluates to (a . b)
;; result:
;; '(a . b)
;; => (A . B)

;; 9.14
;; given the first structure, where the cdr of the list points back to itself,
;; the function will recur endlessly on (dot-prin1 (cdr lst))
;; given the second structure as input, where the car of the list points back
;;   to itself, the function will recur endlessly on (dot-prin1 (car lst))

;; 9.15

(defun hybrid-prin1 (lst)
  "prints a list out in hybrid form"
  (labels ((prin1-cdr (lstcdr)
	     ;; first case, lstcdr is nil so print a right paren
	     (cond ((null lstcdr) (format t ")"))
		   ;; second case, lstcdr is an atom so print a dotted list end
		   ((atom lstcdr) (format t " . ~s)" lstcdr))
		   ;; third case, lstcdr is a list so print a space and its car,
		   ;;   then continue on printing the rest of it.
		   (t (format t " ~s" (car lstcdr))
		      (prin1-cdr (cdr lstcdr))))))
    ;; always print out the left paren... except when it's nil
    ;; first case where lst is nil, just print nil
    (cond ((null lst) (format t "~s" nil))
	  ;; second case, where (car lst) is a non-nil atom --
	  ;;   print left paren, (car lst), and print out (cdr lst)
	  ((atom (car lst))
	   (format t "(~s" (car lst))
	   (prin1-cdr (cdr lst)))
	  ;; third case, where (car lst) is a list.
	  ;;   print out a left paren, then (hybrid-prin1 (car lst)) since it's also a list
	  ;;   then print out (cdr lst)
	  (t (format t "(")
	     (hybrid-prin1 (car lst))
	     (prin1-cdr (cdr lst))))))
