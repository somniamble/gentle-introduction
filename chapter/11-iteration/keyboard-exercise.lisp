;; 11.22.a

(defun complement-base (base)
  "returns the complement value for a base"
  (cond ((eq base 'A) 'T)
	((eq base 'T) 'A)
	((eq base 'G) 'C)
	((eq base 'C) 'G)
	(t 'BAD)))

;; .b
(defun complement-strand (sequence)
  "produces the complement strand of a sequence of DNA"
  (do* ((head (car sequence) (car tail))
	(tail (cdr sequence) (cdr tail))
	(complement (list (complement-base head)) (cons (complement-base head) complement)))
       ((null tail) (nreverse complement))))

(defun zip (lst1 lst2)
  "turns two lists into a single list of pairs (truncated by the shortest list)"
  (do ((rest-lst1 lst1 (rest rest-lst1))
       (rest-lst2 lst2 (rest rest-lst2))
       (zipped nil (cons (list
			  (first rest-lst1)
			  (first rest-lst2))
			 zipped)))
      ((or (null rest-lst1) (null rest-lst2))
       (nreverse zipped))))

;; .c
(defun make-double (sequence)
  "zips together a sequence with its complement"
  (zip sequence (complement-strand sequence)))


;; counts resembles the following:
;; ((a a-count)
;;  (t t-count)
;;  (g g-count)
;;  (c c-count))
(defun make-counts ()
  "creates a base count"
  ;; (list (list 'a 0)
  ;; 	(list 't 0)
  ;; 	(list 'g 0)
  ;; 	(list 'c 0))
  '((a 0)
    (t 0)
    (g 0)
    (c 0)))

(defun update-count! (base counts)
  "destructively increments counts depending on which base is passed in"
  (incf (second
	 (cond ((eq base 'a) (first counts))
	       ((eq base 't) (second counts))
	       ((eq base 'g) (third counts))
	       ((eq base 'c) (fourth counts)))))
  counts)

(defun count-single-strand-bases (sequence)
   (let ((counts (make-counts)))
     (dolist (e sequence counts)
       (update-count! e counts))))

(defun adjust-double-stranded (counts)
  (let* ((a-count (second (first counts)))
	 (t-count (second (second counts)))
	 (c-count (second (third counts)))
	 (g-count (second (fourth counts)))
	 (at-count (+ a-count t-count))
	 (gc-count (+ g-count c-count)))
    `((a ,at-count)
      (t ,at-count)
      (g ,gc-count)
      (c ,gc-count))))

(defun count-double-strand-bases (sequence)
  (let ((counts (make-counts)))
    (dolist (e sequence
	       (adjust-double-stranded counts))
      (update-count! (car e) counts))))

(count-single-strand-bases '(a g t c c c))

(sb-kernel:get-lisp-obj-address (make-counts))
;; same every time, until the function is updated. very curious...
;;  this means, of course, we can't use the function
;;  but why is it that this happens?
;; => 68994649239
;; => 68994649239
;; => 68994649239


;; 11.22.d
(defun count-bases (sequence)
  "counts the bases in a single or double-stranded DNA sequence"
  (if (consp (first sequence))
      (count-double-strand-bases sequence)
      (count-single-strand-bases sequence)))

;; .e
(defun take (n lst)
  "takes the first n elements of lst, or all elements of lst if (> n (length lst))"
  (do ((counter n (- counter 1))
       (tail lst (rest tail))
       (taken nil (cons (first tail) taken)))
      ;; check if counter is 0 or if tail is null, and return the list
      ((or (zerop counter) (null tail)) (nreverse taken))))

(defun prefixp (fst snd)
  "determines if the list `fst` is a prefix of the list `snd`"
  (equal fst (take (length fst) snd)))

;; .f
(defun appearsp (fst snd)
  "determines if the lst `fst` appears anywhere within the list `snd`"
  (do ((len-fst (length fst))
       (tail-snd snd (rest tail-snd)))
      ((> len-fst (length tail-snd)) nil)
    (when (prefixp fst tail-snd)
      (return t))))

;; .g
(defun drop (n lst)
  "drops the first n elements of lst, returning everything after"
  (do ((counter n (- counter 1))
       (tail lst (rest tail)))
      ((or (zerop counter) (null tail)) tail)))

(defun coverp (fst snd)
  "determines if fst, repeated some number of times, constitutes the entirety of snd"
  (do ((len (length fst))
       (tail snd (drop len tail)))
      ;; we are assured by David S. Touretzky that the input will not be nil
      ((null tail) t)
    ;; (format t "~&fst: ~S, tail: ~S" fst tail)
    ;; if fst is not a prefix of tail, break out and return nil
    (unless (prefixp fst tail)
      (return nil))))

;; .h
(defun prefix (n sequence)
  "basically TAKE with another name"
  (take n sequence))

(defun random-base ()
  (nth (random 4) '(a t g c)))

;; test functions
(defun randseq (len)
  "generates a random strand of dna of a given length"
  (do ((seq nil (cons (random-base) seq))
       (counter len (- counter 1)))
      ((zerop counter) (format t "~&using seq ~{~A~}" seq) seq)))

(defun make-repeating-sequence (n sequence)
  "generates a repeating sequence of dna out of a sequence and a number of repeats"
  (do ((seq nil (append sequence seq))
       (counter n (- counter 1)))
      ((zerop counter) seq)))


;; .i
(defun kernel (sequence)
  "attempts to find the shortest prefix of sequence which covers it entirely"
  (do* ((len-kernel 1 (1+ len-kernel))
	(seq-halflen (/ (length sequence) 2))
	(kern (take len-kernel sequence) (take len-kernel sequence)))
       ;; when the length of the test kernel is greater than half the length of the sequence
       ;; we can be certain that the sequence has no kernel smaller than itself
       ((> len-kernel seq-halflen) sequence)
    ;; when we find a test kernel that seems to cover the sequence,
    ;;  we can go ahead and return that
    (when (coverp kern sequence)
      (return kern))))

;; .j draw-dna
(defun repeat-string (string times)
  "generates a repeated string"
  (with-output-to-string (stream)
    (dotimes (i times) (write-string string stream))))

(defun draw-dna (seq)
  "draws a strand of DNA and its complement"
  ;; I shortened mine a little bit because i prefer it this way
  (let* ((len (length seq))
	(complement (complement-strand seq))
	(outer-line (repeat-string "---" len))
	(outer-pad (repeat-string " ! " len))
	(inner-pad (repeat-string " . " len)))
    (format t "~&~A~&~A~&~{ ~A ~}~&~A~&~A~&~{ ~A ~}~&~A~&~A"
	    outer-line outer-pad seq inner-pad
	    inner-pad complement outer-pad outer-line)))

;; so, the strand (a g g t) will resemble:
;; --------------------
;;   !    !    !    !
;;   A    G    G    T
;;   .    .    .    .
;;   .    .    .    .
;;   T    C    C    A
;;   !    !    !    !
;; --------------------

