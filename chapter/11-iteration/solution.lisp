
;; the virgin `dotimes` and `dolist` macros
;; (dotimes (index-var n [result-form])
;;  body)

;; (dolist (index-var list [result-form])
;;  body)

;; dotimes evalueates forms in its body n times, stepping up the index-var in the range [0, n)
;;   afterwards it returns the value built up in [result-form] (defaults to nil)

(dotimes (i 4)
  (format t "~&I is ~S." i))

;; dolist has the same syntax, but it steps through the elements of the list instead
;;   afterwards it returns the value built up in [result-form]

(dolist (x '(red blue green) 'fleurs)
  (format t "~&Roses is ~S." x))

;; you can exit from a loop early using (return &optional <value>)

;; did you know that (return x) is shorthand for (return-from nil x)?
;; (things like dotime, dolist, do, do*, are all block forms
;;   their label is `nil`, so they are returned from when you specifiy (return-from nil)
;; things like functions are named blocks, so if you have a function `foo` and an inner loop
;; you can say (return-from 'foo x) from inside the loop to break from the whole thing

;; 11.1
(defun it-member (x set)
  "iterates through set, returning nil if no element equals x, or t if any element equals x"
  (dolist (i set nil)
    (when (equal x i) (return t))))

;; 11.2
(defun it-assoc (x alist)
  "iterates through an alist, and returns nil or the element corresponding to x"
  (dolist (a alist nil)
    (when (equal x (car a)) (return a))))

;; 11.3
;; torture test
(defun check-all-odd (list-of-numbers)
  (dolist (e list-of-numbers t)
    (format t "~&Checking ~S..." e)
    (if (not (oddp e)) (return nil))))

(defun re-check-all-odd (list-of-numbers)
  "recursively checks if all numbers in list-of-numbers are odd"
  (if (null list-of-numbers) t
      (progn (format t "~&Checking ~S..." (car list-of-numbers))
	     (if (oddp (car list-of-numbers))
		 (re-check-all-odd (cdr list-of-numbers))
		 nil))))

;; RECURSIVE VS. ITERATIVE SEARCH
;; iterative vs. recursive search
;; iterative search is a little more concise, and the end test is implicit
;; generally, we prefer iterative search on a flat list
;;  and we prefer recursive search on a nested list (or tree)

;; BUILDING UP A LIST WITH ASSIGNMENT
;; examples:
(defun it-fact (n)
  (let ((prod 1))
    (dotimes (i n prod)
      (setf prod (* prod (+ i 1))))))

(defun it-intersection (x y)
  (let ((result-set nil))
    (dolist (element x result-set)
      (when (it-member element y)
	(push element result-set)))))

;; 11.4
(defun it-length (lst)
  "iteratively determines the length of lst"
  (let ((len 0))
    (dolist (_ lst len)
      (incf len))))

;; 11.5
(defun it-nth (n lst)
  "iteratively retrieves the nth element of lst"
  (let ((idx n))
    (dolist (e lst)
      (if (zerop idx) (return e)
	  (incf idx -1)))))

;; 11.6
(defun it-union (x y)
  "iteratively produces the union of x and y"
  (let ((result-set y))
    (dolist (element x result-set)
      (unless (it-member element result-set)
	(push element result-set)))))

;; 11.7
;; it-intersection returns elements in reverse order from the first input
;;   say the x and y are the same set...
;;   then it-intersection pushes (first x) onto nil
;;   then it-intersection pushes (second x) onto (first x)
;;   then it-intersection pushes (third x) onto ((second x) (first x))
;;   and so on

;; 11.8
(defun it-reverse (lst)
  "iteratively reverses a list"
  (let ((reversed nil))
    (dolist (x lst reversed)
      (push x reversed))))

;; the chad `do` macro
;; 'the most powerful iteration form in lisp'
;; binds any number of variables
;; steps and number of index variables any way you like
;; lets you specify your own test to decide when to exit the loop
;; probably fucks your mom too

;; (do ((var1 init1 [update1])
;;      (var2 init2 [update2])
;;      ...)
;;     (test action-1 ... action-n)
;; body)
;; each var is set to its initial
;; then the test form is evaluated
;;    if the result is true, do evaluates the termination actions and returns
;;      the value of the last one (action-n)
;; otherwise `do` evaluates the body-forms in order
;; when `do` reaches the end of the body forms, it begins the next iteration
;;   each variable is updated by setting it to the value of the update expression;
;;   (if the update expression is left off the the variable is unchanged)

;; here's a toy function
(defun launch (n)
  (do ((cnt n (- cnt 1)))
      ((zerop cnt) (format t "blasdoff !! :DDD"))
    (format t "~S..." cnt)))

;; 11.9
(defun chad-all-odd (lst)
  (do ((tail lst (cdr tail)))
      ((null tail) t)
    (format t "~&Checking ~S..." (car tail))
    (when (evenp (car tail)) (return nil))))

;; 11.10
(defun virgin-launch (n)
  (dotimes (i n (format t "blasdoff! ! ! XDDD"))
    (format t "~S..." (- n i))))

;; if all we need to do is count, or iterate over the elements of a list
;;  then dotimes and dolist will be more concise.
;;  however, if we need to build up some kind of result, do is probably better

;; the gigachad `do*`
;;  it's just like the let* version of let for do

;; 11.11
(defun find-largest (list-of-numbers)
  (let ((largest (first list-of-numbers)))
    (dolist (element (rest list-of-numbers)
		     largest)
      (when (> element largest)
	(setf largest element)))))

(defun do-find-largest (list-of-numbers)
  (do* ((successive list-of-numbers (cdr successive))
	(head (car list-of-numbers) (car successive))
	(largest head (max largest head)))
       ((equal 1 (length successive)) largest)))

;; 11.12
(defun do-power-of-two (n)
  "calculates the power of two using the power of do :o)"
  (do ((result 1 (* result 2))
       (iter n (- iter 1)))
      ((zerop iter) result)))

;; 11.13
(defun first-non-integer (x)
  "return the first non-integer element of x"
  (do* ((z x (rest z))
	(zl (first z) (first z)))
       ((null z) 'none)
    (unless (integerp zl)
      (return zl))))


(defun ffo-with-dolist (x)
  (dolist (i x 'none)
    (unless (integerp i)
      (return i))))

;; 11.14
;; changing to do* to do would introduce a bug when we attempt to reference
;;   `x` in the initialization of `e`

(defun ffo-with-do (x)
  (do ((z x (rest z))
       (e (first x) (first z)))
      ((null z) nil)
    (format t "~&e is ~S, z is ~S" e z)
    (if (oddp e) (return e))))

;; when we try to find an odd number in the very last cdr of the list,
;;   then we get to the second to last position (say the list is (2 4 6 7))
;;   so e is 6, and z is (7)
;;   6 is not odd, so we loop again
;;   then, we iterate again -- and we update z and x in parallel
;;   so, z updates from (7) to nil
;;   and e updates from (car (6 7)) to (car 7)
;;   so the end condition triggers and we "complete" the loop prematurely

;; 11.10 - INFINITE LOOPS WITH DO

(defun read-a-number ()
  (do ((answer nil))
      ;; specify nil as the end condition
      ;; so, the end condition will never be true... we will loop foreber
      (nil)
    (format t "~&Please type a number: ")
    (setf answer (read))
    (if (numberp answer)
	(return answer))
    (format t
	    "~& sorry, ~S is not a number. try again."
	    answer)))



;; we could also do like a y-or-no-p kind of thing

(defun lizard ()
  (do ((answer nil))
      ;; specify nil as the end condition
      ;; so, the end condition will never be true... we will loop foreber
      ((not (y-or-n-p "do you wish to continue")))
    (format t "~&my lizard beging to show")))

;; 11.16
;; Let is a macro which binds certain values to certain variables in an execution context

;; DO is a macro which does the same, but also provides looping facilities,
;;   including the ability to update those variables per iteration,
;;   an expression to end looping
;;   and a body which is evaluated once per loop

;; 11.17
;; (dotimes (i 5 i) (format t "~&I = ~S" i))
;;  => 5 -- because we return the final value of i

;; 11.18
(do ((i 0 (1+ i)))
    ((= i 5) i)
  (format t "~&I = ~S" i))

;; 11.19
;; for DO explressions, the order in which variables are defined doesn't matter
;;  each expression is evaluated in parallel

;; 11.20
;; I don't see any reason why not --

;; 11.21

(defun it-fib (n)
  (do* ((i n (- i 1))
	(prev-2 0 prev-1)
	(prev-1 1 curr)
	;; start at fib(1)
	;; then, let the 
	(curr 1 (+ prev-2 prev-1)))
       ((< i 2) curr)))

;; FUNCTIONS COVERED:
;; iteration macros:
;;   DOTIMES, DOLIST, DO, DO*

;; Special functions for block structure:
;;   BLOCK, RETURN-FROM

;; Ordinary function for exiting a block named NIL:
;;   RETURN
