;; PROG1, PROG2, PROGN
;; prog1 evaluates any number of expressions, and returns the value of the first
(defparameter x nil)
(prog1 (setf x 'foo)
  (setf x 'bar)
  (setf x 'baz)
  (format t "~&X is ~S" x))
;; X is BAZ
;; 
;; FOO

;; prog2 evaluates any number of expressions, and returns the value of the second
(prog2 (setf x 'foo)
  (setf x 'bar)
  (setf x 'baz)
  (format t "~&X is ~S" x))
;; 
;; X is BAZ
;; 
;; BAR

;; progn evaluates any number of expressions, and returns the value of the last
(progn (setf x 'foo)
  (setf x 'bar)
  (setf x 'baz)
  (format t "~&X is ~S" x))
;; X is BAZ
;; 
;; NIL
;; generally these days, we use LET instead

;; LAMBDA KEYWORDS

;; optional arguments, bound to nil if they aren't provided at function call time
(defun eg-optional (x &optional y z)
  (format t "~&x is ~S,~&y is ~S,~&z is ~S" x y z))
(eg-optional 'a)
;; x is A,
;; y is NIL,
;; z is NIL
(eg-optional 'a 'b)
;; x is A,
;; y is B,
;; z is NIL
(eg-optional 'a 'b 'c)
;; x is A,
;; y is B,
;; z is C

;; rest arguments. 
;; of course this one will divide by zero if we don't supply the first
;; (defun average (&rest args)
;; ;;   (format t "averaging over the list ~S" args)
;; ;;   (/ (reduce #'+ args)
;; ;;      (length args)
;; ;;      1.0))

;; rest-args is bound to a list of the rest of the arguments
(defun average (first-term &rest rest-args)
  (let ((args (cons first-term rest-args)))
    (format t "averaging over the first term ~S and rest of args ~S" first-term rest-args)
    (/ (reduce #'+ args)
       (length args)
       1.0)))

(average 5)
;; averaging over the first term 5 and rest of args NIL
;; 
;; 5.0
(average 1 2 3 4 5 6 7 8 9 10)
;; averaging over the first term 1 and rest of args (2 3 4 5 6 7 8 9 10)
;; 
;; 5.5

;; KEYWORD ARGUMENTS
;; (member x y :test #'equal)
;; flavor is bound to 'vanilla by default

;; syrup is bound to 'hot-fudge by default

;; nuts, cherries, and whipped-cream are all nil by default
(defun make-sundae (name &key (flavor 'vanilla)
			   (syrup 'hot-fudge)
			   nuts
			   cherries
			   whipped-cream)
  (list 'sundae
	(list 'for name)
	(list flavor 'with syrup 'syrup)
	(list 'toppings '=
	      (remove nil
		      (list (and nuts 'nuts)
			    (and cherries 'cherries)
			    (and whipped-cream
				 'whpped-cream))))))

(make-sundae 'somni :flavor 'punishment :syrup 'hell :nuts t)
;; (SUNDAE
;; ;;  (FOR SOMNI)
;; ;;  (PUNISHMENT WITH HELL SYRUP)
;; ;;  (TOPPINGS = (NUTS)))

;; auxilliary variables
;; variables after &aux are like statements in a LET form
;; I think I rather like them, actually
(defun aux-average (first-term &rest rest-args
		    &aux
		      (args (cons first-term rest-args))
		      (len (length args)))
  (format t "averaging ~S" args)
  (/ (reduce #'+ args) len 1.0))

(aux-average 1 2 3 4)
;; averaging (1 2 3 4)
;; 
;; 2.5
