;; defvar does not overwrite a value if it's already set
;; that's what defparameter does
(defvar *total-glasses* 0)


(defun sell (n)
  "Ye olde lemonade stand..."
  (setf *total-glasses* (+ *total-glasses* n))
  (format t "~&That makes ~a glasses so far today..."
	  *total-glasses*)
  *total-glasses*)

;; 10.1
;; if we neglected to initialize *total-glasses* then we would have gotten an
;; error when we attempted to setf it to itself + n, since it's not defined yet...
;; similar story if it was set to foo, we would get a type error

;; 10.2
(defun sell-incf (n)
  (incf *total-glasses* n)
  (format t "~&That makes ~a glasses so far today..."
	  *total-glasses*)
  *total-glasses*)



;; 10.3
(defvar *friends* nil)

(defvar *met-already* 0)

(defun meet (person)
  (cond ((equal person (first *friends*))
	 (incf *met-already*)
	 'we-just-met)
	((member person *friends*)
	 (incf *met-already*)
	 'we-know-each-other)
	(t (push person *friends*)
	   'please-to-meet-ya)))

;; 10.4
(defun forget (friend)
  "forgets a friend from the list of friends"
  (let ((prev-number (length *friends*))
	(new-list (remove friend *friends*)))
    (if (= prev-number (length new-list))
	'we-were-not-friends
	(setf *friends* new-list))))


;; tic-tac-toe
;; 1 | 2 | 3
;; ---------
;; 4 | 5 | 6
;; ---------
;; 7 | 8 | 9

(defun make-board ()
  (list 'board 0 0 0 0 0 0 0 0 0))

(defun convert-to-letter (v)
  (cond ((equal v 1) 'O)
	((equal v 10) 'X)
	(t " ")))

;; 9.5
(defun print-board (state)
  "prints a tic-tac-toe board"
  (let ((printable
	  (mapcar #'convert-to-letter
		  (cdr state))))
    (format t "~{~& ~a | ~a | ~a ~&~^-----------~}" printable)))

(defun make-move (player pos board)
  (setf (nth pos board) player)
  board)


(defparameter *computer* 10)
(defparameter *human* 1)

(defvar *triplets*
  '((1 2 3) (4 5 6) (7 8 9)
    (1 4 7) (2 5 8) (3 6 9)
    (1 5 9) (3 5 7)))

(defun sum-triplet (board triplet)
  "sums the positions on the board indicated by the triplet"
  (+ (nth (first triplet) board)
     (nth (second triplet) board)
     (nth (third triplet) board)))

(defun compute-sums (board)
  "computes the sums for each triplet on the board"
  (mapcar (lambda (triplet) (sum-triplet board triplet))
	  *triplets*))

(defun winner-p (board)
  "determines if the board has a winner (any triplets summing to 3 or 30"
  (let ((sums (compute-sums board)))
    (or (member (* 3 *computer*) sums)
	(member (* 3 *human*) sums))))

(defun play-one-game ()
  "begins a game of tic-tac-toe" 
  (if (y-or-n-p  "Would you like to go first? ")
      (opponent-move (make-board))
      (computer-move (make-board))))

(defun opponent-move (board)
  (let* ((pos (read-a-legal-move board))
	 (new-board (make-move
		     *human*
		     pos
		     board)))
    (print-board new-board)
    (cond ((winner-p new-board)
	   (format t "~&You win!"))
	  ((board-full-p new-board)
	   (format t "~&Tie game."))
	  (t (computer-move new-board)))))

(defun board-full-p (board)
  "determines if the board is full or not"
  (not (member 0 board)))


(defun is-occupied-p (pos board)
  (/= (nth pos board) 0))

(defun valid-move-p (pos board)
  "determines if a move is valid or not"
  (and (numberp pos)
       (and (> pos 0) (<= pos 9))
       (not (is-occupied-p pos board))))

(defun read-a-legal-move (board)
  "determines if a move is numeric, in the range (1,9), and not an occupied position"
  (format t "~&Your move: ")
  (let ((pos (read)))
    (if (valid-move-p pos board)
	;; move is valid, return pos
	pos
	(progn (format t "~&Invalid move ~a" pos)
	       (read-a-legal-move board)))))

(defun computer-move (board)
  (let* ((best-move (choose-best-move board))
	 (pos (first best-move))
	 (strategy (second best-move))
	 (new-board (make-move *computer* pos board)))
    (format t "~&My move: ~S" pos)
    (format t "~&My strategy: ~A~%" strategy)
    (print-board new-board)
    (cond ((winner-p new-board)
	   (format t "~&I win."))
	  ((board-full-p new-board)
	   (format t "~&Tie game."))
	  (t (opponent-move new-board)))))

(defun choose-best-move (board)
  "Picks the best position given the current state of the board"
  (or (make-three-in-a-row board)
      (block-opponent-win board)
      (block-squeeze-play board)
      (block-two-on-one board)
      (offensive-play board)
      (random-move-strategy board)))

(defun random-move-strategy (board)
  "picks a random empty position from the board and states the strategy"
  (list (pick-random-empty-position board)
	"random move"))

(defun pick-random-empty-position (board)
  "picks a random position from the board, if it's empty returns pos, otherwise calls again"
  (let ((pos (+ 1 (random 9))))
    (if (zerop (nth pos board))
	pos
	(pick-random-empty-position board))))

(defun make-three-in-a-row (board)
  "makes 3 in a row"
  (let ((pos (win-or-block board
			   (* 2 *computer*))))
    (and pos (list pos "make three in a row"))))

(defun block-opponent-win (board)
  "blocks the opponent making 3 in a row"
  (let ((pos (win-or-block board
			   (* 2 *human*))))
    (and pos (list pos "block opponent win"))))


(defun win-or-block (board target-sum)
  "returns a position in a triplet that will either win or block"
  (let ((triplet (find-if
		  (lambda (triplet)
		    (= (sum-triplet board triplet)
		       target-sum))
		  *triplets*)))
    (when triplet
      (find-empty-position board triplet))))

(defun find-empty-position (board positions)
  "finds the first empty position in a given triplet"
  (find-if (lambda (pos) (zerop (nth pos board)))
	   positions))


;; 10.5
;; (defun ugly (x y)
;; ;;   (when (> x y)
;; ;;     (setf temp y)
;; ;;     (setf y x)
;; ;;     (setf x temp))
;; ;;   (setf avg (/ (+ x y) 2.0))
;; ;;   (setf pct (* 100 (/ avg y)))
;; ;;   (list 'average avg 'is pct 'percent 'of 'max y))

(defun pretty (x y)
  (let* ((bigger (max x y))
	 (average (/ (+ x y) 2.0))
	 (percent (* 100 (/ average bigger))))
    (list 'average average 'is percent 'of 'max bigger)))

;; 10.6
;; if x = nil
;; then (push x x) is (nil)
;; x = (nil)
;; then (push x x) is
;; x = ((nil) nil)
;; then (push x x) is
;; x = (((nil) nil) (nil) nil)

;; 10.7
;; (setf (length x) 3)
;; what's wrong with this?
;; well, (length x) is just a number that is the result of an evaluation
;; it's not an address, it's not a generalized variable,
  ;; it's not a place a pointer may be stored

;; keyboard exercise
;; squeeze plays and two-on-two

;; 10.8.a
(defparameter *corners* '(1 3 7 9))
(defparameter *sides* '(2 4 6 8))

;; .b

(defun squeeze-play-p (board triplet)
  "determines if there is currently a squeeze play ongoing in this triplet on this board"
  (and (= (+ (* 2 *human*) *computer*) (sum-triplet board triplet))
       (= (nth 5 board) *computer*)))

(defun block-squeeze-play (board)
  "attempts to block a squeeze play"
  (when (or (squeeze-play-p board '(1 5 9))
	  (squeeze-play-p board '(3 5 7)))
      (list (find-empty-position board *sides*) "block squeeze play")))

;; .c

(defun two-on-one-p (board triplet)
  "determines if there is currently a squeeze play ongoing in this triplet on this board"
  (and (= (+ (* 2 *human*) *computer*) (sum-triplet board triplet))
       (or (= (nth 1 board) *computer*)
	   (= (nth 9 board) *computer*))))

(defun block-two-on-one (board)
  "attempts to block a two-on-one play"
  (when (or (two-on-one-p board '(1 5 9))
	    (two-on-one-p board '(3 5 7)))
    (list (find-empty-position board *corners*) "block two-on-one")))

;; .e

;; conditions where we can set up a two-on-one play:
;; we begin in a corner or in the center
;; opponent has taken a corner in the same diagonal
;; then we take the open space left on the diagonal

;; conditions where we can set up a squueze play:
;; we begin in the corner 
;; opponent takes center
;; then we take the open space left on the diagonal

(defun computer-p (position-value)
  "determines if the board position value is computer-controlled"
  (= position-value *computer*))

(defun human-p (position-value)
  "determines if the board position value is human-controlled"
  (= position-value *human*))

(defun empty-p (position-value)
  "determines if the board position value is empty"
  (zerop position-value))

(defun make-offensive-play (board triplet)
  (let ((sum (sum-triplet board triplet))
	(on-center (nth (second triplet) board)))
    (when (= sum (+ *computer* *human*))
      (list (find-empty-position board triplet)
	    (if (human-p on-center)
		"set up squeeze play"
		"set up two-on-one")))))

(defun offensive-play (board)
  "attempts to set up an offensive play on the board"
  (or (make-offensive-play board '(1 5 9))
      (make-offensive-play board '(3 5 7))))


;; tic-tac-toe
;; 1 | 2 | 3
;; ---------
;; 4 | 5 | 6
;; ---------
;; 7 | 8 | 9

;; (defvar *triplets*
;;   '((1 2 3) (4 5 6) (7 8 9)
;;     (1 4 7) (2 5 8) (3 6 9)
;;     (1 5 9) (3 5 7)))

;; advanced topics:
;; creating a circular list structure
(defvar circ (list 'foo))

(setf (cdr circ) circ)

;; destructive operations on lists

;; nconc, conc but the last cons cell of the first input is set to the first cell
;; nsubst, destructive replacement of elements based on a predicate
;; push, pop, cons and car but destructive

;; nreverse, nunion, nintersection, nset-difference

;; here's some shit
(defvar *things* '((object1 large green shiny cube)
		   (object2 small red dull meltal cube)
		   (object3 red small dull plastic cube)))


(defun rename! (obj newname)
  (setf (car (assoc obj *things*)) newname))

(defun add! (obj prop)
  (nconc (assoc obj *things*) (list prop)))


;; 10.9
(defun chop! (lst)
  (unless (null lst)
    (setf (cdr lst) nil)))

;; 10.10
(defun ntack! (lst sym)
  (nconc lst (list sym)))

;; 10.11

;;  first cell
;;   / \
;;  a  /\
;;    b /\
;;     c  \
;;         first cell

;; 10.12
;; (defvar h '(hi ho))

;; (append h h) => (hi ho hi ho)
;; h => hi ho
;;
;; (nconc h h) => (hi ho # hi ho # hi ho # ...) 
;; why? because the cdr of (last h) is now h
;;   which makes h a circular list
