(defun emphasize3 (x)
  (cond ((equal (car x) 'good) (cons 'great (cdr x)))
	((equal (car x) 'bad) (cons 'awful (cdr x)))
	(t (cons 'very x))))

(emphasize3 '(good people here))
;;; =>(GREAT PEOPLE HERE)
(emphasize3 '(bad day today))
;;; =>(AWFUL DAY TODAY)
(emphasize3 (emphasize3 '(bad day today)))
;;; =>(VERY AWFUL DAY TODAY)

;;; 4.9
#+nil					;bad.
(defun make-odd (x)
  (cond (t x)
	((not (oddp x)) (+ x 1))))

(defun make-odd (x)
  (cond ((not (oddp x)) (+ x 1))
	(t x)))

;;; 4.10
;; version 1
#+nil
(defun constrain (x max min)
  (cond ((< x min) min)
	((> x max) max)
	(t x)))
(constrain 3 50 -10)
;;; =>3
(constrain 10 1 -1)
;;; =>1

(defun constrain (x max min)
  (if (< x min) min
      (if (> x max) max
	  x)))

;;; 4.11
(defun firstzero (xs)
  (cond ((zerop (car xs)) "first")
	((zerop (cadr xs)) "second")
	((zerop (caddr xs)) "third")
	(t "none")))

(firstzero '(3 0 4))
;;; =>"second"
;;; if you were to call firstzero with 3 arguments, you would get an invalid number of arguments error

;;; 4.12
(defun cycle (x)
  (if (equal x 99) 1
      (+ 1 x)))

;;; 4.13
;;; we could extend this by doing powers, division, digital cumulation, radicals...
(defun howcompute (x y result)
  (cond ((equal result (+ x y)) 'sum-of)
	((equal result (* x y)) 'product-of)
	(t 'beats-me)))

(howcompute 2 2 4)
;;; SUM-OF

(howcompute 3 3 9)
;;; =>PRODUCT-OF

(howcompute 3 3 12)
;;; =>BEATS-ME

;;; 4.14
;; and / or are macros.... they return the first value that lets them stop thinking
(and 'fee 'fie 'foe) 			; => FOE
(or 'fee 'fie 'foe) 			; => FEE
(or nil 'foe nil) 			; => FOE
(and 'fee 'fie nil) 			; => nil
(and (equal 'abc 'abc) 'yes) 		; => YES
(or (equal 'abc 'abc) 'yes) 		; => t

;;; 4.15
(defun geq (x y)
  (or (= x y)
      (> x y)))

;; 100 geqs....
(geq 100 100)
;;; =>T

(geq 100 101)
;;=>NIL

(geq 101 100)
;;; =>T

;;; 4.16
;;; could have done nested ifs...
(defun numfuck (x)
  (cond ((and (oddp x) (> x -1)) (* x x))
	((and (oddp x) (< x 0)) (* 2 x))
	(t (/ x 2))))

(numfuck 3)
;;; =>9

(numfuck -15)
;;; =>-30

(numfuck -10)
;;; =>-5

;;; 4.17
(defun proper-gender (gender age)
  (or (and (equal age 'child) (or (equal gender 'boy) (equal gender 'girl)))
      (and (equal age 'adult) (or (equal gender 'man) (equal gender 'woman)))))

(proper-gender 'man 'child)
;;; =>NIL

(proper-gender 'woman 'child)
;;; =>NIL

(proper-gender 'man 'adult)
;;; =>T
;; and so on and so on

;;; 4.18
(defun p1-wins (p1 p2)
  (or (and (equal p1 'rock) (equal p2 'scissors))
      (and (equal p1 'paper) (equal p2 'rock))
      (and (equal p1 'scissors) (equal p2 'paper))))

(p1-wins 'rock 'paper)
;;; =>NIL

(p1-wins 'rock 'scissors)
;;; =>T

(defun rps (p1 p2)
  (cond ((equal p1 p2) 'TIE)
	((p1-wins p1 p2) 'FIRST-WINS)
	(t 'SECOND-WINS)))

(rps 'rock 'paper)
;;; =>SECOND-WINS

(rps 'paper 'rock)
;;; =>FIRST-WINS

(rps 'scissors 'scissors)
;;; =>TIE

;;; 4.20
#+nil 					;compare written with cond.
(defun compare (x y)
  (cond ((equal x y) 'numbers-are-the-same)
	((< x y) 'first-is-smaller)
	((> x y) 'first-is-bigger)))

#+nil 					;compare written with if
(defun compare (x y)
  (if (equal x y) 'numbers-are-the-same
      (if (< x y) 'first-is-smaller
	  'first-is-bigger)))

(compare 5 5)
;;; =>NUMBERS-ARE-THE-SAME
(compare 5 6)
;;; =>FIRST-IS-SMALLER
(compare 6 5)
;;; =>FIRST-IS-BIGGER

(defun compare (x y)
  (or (and (equal x y) 'numbers-are-the-same)
      (and (< x y) 'first-is-smaller)
      'first-is-larger))
(compare 5 5)
;;; =>NUMBERS-ARE-THE-SAME
(compare 5 6)
;;; =>FIRST-IS-SMALLER
(compare 6 5)
;;; =>FIRST-IS-LARGER

;;; 4.21
#+nil
(defun gtest (x y)
  (or (> x y)
      (zerop x)
      (zerop y)))

(gtest 5 6)
;;; =>NIL
(gtest 6 6)
;;; =>NIL
(gtest 6 5)
;;; =>T

(gtest 0 1)
;;; =>T
(gtest -5 0)
;;; =>T

#+nil
(defun gtest (x y)
  (if (> x y) t
      (if (zerop x) t
	  (zerop y))))

(gtest 5 6)
(gtest 6 6)
(gtest 6 5)
(gtest 0 1)
(gtest -5 0)

(defun gtest (x y)
  (cond ((> x y) t)
	((zerop x) t)
	(t (zerop y))))

;;; 4.22

#nil
(defun boilingp (temp scale)
  (cond ((equal scale 'FAHRENHEIT) (> temp 212))
	((equal scale 'CELSIUS) (> temp 100))))

#+nil
(defun boilingp (temp scale)
  (if (equal scale 'fahrenheit) (> temp 212)
      (if (equal scale 'celsius) (> temp 100))))

(defun boilingp (temp scale)
  (or (and (equal scale 'fahrenheit) (> temp 212))
      (and (equal scale 'celsius) (> temp 100))))

(boilingp 50 'celsius)
(boilingp 50 'fahrenheit)
(boilingp 101 'celsius)
(boilingp 101 'fahrenheit)
(boilingp 213 'celsius)
(boilingp 213 'fahrenheit)

;;; 4.23 - 4.27 done pen-on-paper

;;; 4.28
(if (oddp 5) (evenp 7)
    'FOO)

;;;  so this fails, because the true-part is a predicate which evaluates to nil
(or (and (oddp 5) (evenp 7))
    'foo)

(or (and (oddp 5) (evenp 7))
    (and (not (oddp 5)) 'foo))

;;;; aside
(defun my-abs (x)
  (if (< x 0) (- x) x))

;;;  4.29
#+nil
(defun logical-and (x y)
  (and x y t))

#+nil
(defun logical-and (x y)
  (if x (if y t)))

(defun logical-and (x y)
  (cond ((not x) nil)
	((not y) nil)
	(t t)))

(logical-and 5 6)
;;; =>T
(logical-and nil 6)
;;; =>NIL

;;; 4.30
#+nil
(defun logical-or (x y)
  (and (or x y) t))

#+nil					
(defun logical-or (x y)
  (if x t
      (if y t)))

(defun logical-or (x y)
  (cond (x t)
	(y t)))

(logical-or 5 6)
(logical-or nil 6)
(logical-or 5 nil)
(logical-or nil nil)

;;; 4.33
;;; there are 8 lines in this truth table.
(defun logical-if (x y z)
  (if x (and y t)
      (and z t)))

(logical-if t   t   t)
(logical-if t   t   nil)
(logical-if t   nil t)
(logical-if t   nil nil)
(logical-if nil t   t)
(logical-if nil t   nil)
(logical-if nil nil t)
(logical-if nil nil nil)

;;; 4.35
(defun demorgan-and (x y z)
  (not (or (not x) (not y) (not z))))

(demorgan-and t   t   t)
(demorgan-and t   t   nil)
(demorgan-and t   nil t)
(demorgan-and t   nil nil)
(demorgan-and nil t   t)
(demorgan-and nil t   nil)
(demorgan-and nil nil t)
(demorgan-and nil nil nil)

(defun demorgan-or (x y z)
  (not (and (not x) (not y) (not z))))

;;; 4.37
(defun nand (x y) (not (and x y)))

(defun not2 (x) (nand x x))

(defun nand-and (x y)
  (nand (nand x y) (nand x y)))

(format t "nand T   T   = ~a~%nand T   NIL = ~a~%nand NIL T   = ~a~%nand NIL NIL = ~a~%"
	(nand t t)
	(nand t nil)
	(nand nil t)
	(nand nil nil))
;; nand T   T   = NIL
;; nand T   NIL = T
;; nand NIL T   = T
;; nand NIL NIL = T

(format t "nand-and T   T   = ~a~%nand-and T   NIL = ~a~%nand-and NIL T   = ~a~%nand-and NIL NIL = ~a~%"
	(nand-and t t)
	(nand-and t nil)
	(nand-and nil t)
	(nand-and nil nil))

(defun nand-or (x y)
  (nand (nand x x) (nand y y)))

(format t "nand-or T   T   = ~a~%nand-or T   NIL = ~a~%nand-or NIL T   = ~a~%nand-or NIL NIL = ~a~%"
	(nand-or t t)
	(nand-or t nil)
	(nand-or nil t)
	(nand-or nil nil))
;;; =>nand-or T   T   = T
;; nand-or T   NIL = T
;; nand-or NIL T   = T
;; nand-or NIL NIL = NIL

;;; 4.38
(defun nor (x y)
  (not (or x y)))
(format t "~%nor T   T   = ~a~%nor T   NIL = ~a~%nor NIL T   = ~a~%nor NIL NIL = ~a~%"
	(nor t t)
	(nor t nil)
	(nor nil t)
	(nor nil nil))

(defun not3 (x)
  (nor x x))

;; not3 is constructed using nor.
(defun nor-or (x y)
  (not3 (nor x y)))

(format t "~%nor-or T   T   = ~a~%nor-or T   NIL = ~a~%nor-or NIL T   = ~a~%nor-or NIL NIL = ~a~%"
	(nor-or t t)
	(nor-or t nil)
	(nor-or nil t)
	(nor-or nil nil))

(defun nor-and (x y)
  (nor (not3 x) (not3 y)))

(format t "~%nor-and T   T   = ~a~%nor-and T   NIL = ~a~%nor-and NIL T   = ~a~%nor-and NIL NIL = ~a~%"
	(nor-and t t)
	(nor-and t nil)
	(nor-and nil t)
	(nor-and nil nil))

#+nil
(defun nor-nand (x y)
  (not3 (nor-and x y)))
;; full form:
(defun nor-nand (x y)
	(nor-nand t t)
	(nor-nand t nil)
	(nor-nand nil t)
	(nor-nand nil nil))
