;;; 5.6

(defun throw-die ()
  "returns a random number between 1 and 6, inclusive"
  (+ 1 (random 6)))

(defun dice-equal (dice val)
  "determines if the sum of a throw (2 dice) is equal to a given value"
  (equal (sum dice) val))

(defun sum (dice)
  "sums up a throw of dice"
  (reduce #'+ dice))

(defun throw-dice ()
  "generates a list of 2 die throws"
  (list (throw-die) (throw-die)))

(defun snake-eyes-p (dice)
  "determines if a dice throw is equal to 2"
  (dice-equal dice 2))

(defun boxcars-p (dice)
  "determines if a dice throw is equal to 12"
  (dice-equal dice 12))

(defun instant-win-p (dice)
  "determines if a dice throw is an instant win (sum 7 or 11)"
  (or (dice-equal dice 7)
      (dice-equal dice 11)))

(defun instant-loss-p (dice)
  "determines if a dice throw is an instant loss (sum 2, 3, or 12)"
  (or (snake-eyes-p dice)
      (boxcars-p dice)
      (dice-equal dice 3)))

(defun dice-throw-value (dice)
  "formats the first part of an output message: (throw <die 1> and <die 2>)"
  `(throw ,(car dice) and ,(cadr dice)))

(defun throw-symbol (dice)
  "formats the second part of the output message for the first throw"
  (cond ((snake-eyes-p dice) 'snakeyes)
	((boxcars-p dice) 'boxcars)
	(t (sum dice))))

(defun end-message (dice)
  "formats the end of the output message, telling the player if they won or lost or what their point is"
  (cond ((instant-win-p dice) '(you win))
	((instant-loss-p dice) '(you lose))
	(t `(your point is ,(sum dice)))))

(defun say-throw (dice)
  "says the value for a given dice throw -- what the dice are, what the sum is, if they win, lose, or take point"
  `(,@(dice-throw-value dice) -- ,(throw-symbol dice) -- ,@(end-message dice)))

(defun point-symbol (dice goal)
  "outputs whether the player wins, loses, or must throw again when trying for a point"
  (cond ((dice-equal dice 7) '(you lose))
	((dice-equal dice goal) '(you win))
	(t '(throw again))))

(defun try-for-point (goal)
  "throws two dice, outputs their values, their sum, and whether the player wins (sum equals point), loses, or must throw again"
  (let ((dice (throw-dice)))
    `(,@(dice-throw-value dice) -- ,(sum dice) -- ,@(point-symbol dice goal))))
