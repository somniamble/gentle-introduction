
;; making our own custom print functions
(defun print-starship (x stream depth)
  (format stream "#<STARSHIP ~A>"
	  (starship-name x)))

;; (lambda (x stream depth)
 ;; ;;   format stream "#<STARSHIP ~A>"
 ;; ;;   (starship-name x))

;; notice that it's
;; (starship (:print-function print-starship))
;; 
(defstruct (starship (:print-function print-starship))
  (name nil)
  (captain)
  (speed 0)
  (condition 'green)
  (shields 'down))

(defun print-captain (x stream depth)
  (format stream "#<CAPTAIN ~A>"
	  (captain-name x)))

(defstruct (captain (:print-function print-captain))
  name
  age
  ship)

(defparameter kirk (make-captain :name "James T. Kirk"
				 :age 35
				 :ship nil))

(defparameter enterprise (make-starship :name "USS Enterprise"
					:captain kirk))
(setf (captain-ship kirk) enterprise)


;; Equality of Structures

(equal (make-starship) (make-starship))
;; NIL

(equalp (make-starship) (make-starship))
;; T


;; Subclassing

(defstruct ship
  (name nil)
  (captain nil)
  (crew-size 0))

;; defstruct name-and-options &optional slot-descriptions
(defstruct (starship (:include ship)
		     (:print-function print-starship))
  (weapons 'unpowered)
  (shields 'down))

(defstruct (supply-ship (:include ship))
  (cargo 'empty))

(defparameter z1 (make-starship
		  :captain "Zed!"))

(defparameter z2 (make-supply-ship
	  :captain "Fuckhead"))

(ship-captain z1)
"Zed!"
(starship-captain z1)
"Zed!"
(supply-ship-captain z1)
