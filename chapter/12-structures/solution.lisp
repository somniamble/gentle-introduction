;;; TYPEP - determine if the argument is an instance of the given type

(typep 3 'number)
;; T

(typep 3 'integer)
;; T

(typep 3 'float)
;; NIL

;;; TYPE-OF - return the symbol representing the type of the argument

(type-of 'aardvark)
;; SYMBOL


(type-of 3.5)
;; SINGLE-FLOAT

(type-of '(oh neat))
;; CONS

(type-of "Oh damn")
;;(SIMPLE-ARRAY CHARACTER (7))

(type-of nil)
;; NULL

;; THE COMMON LISP TYPE TREE

;;                    T
;;      -----------COMMON-------------
;;     /           /  |  \            \
;;    LIST     ----   |   ---          \
;;  /    |    /       |      \          \
;; CONS  | SYMBOL   NUMBER   STREAM   ARRAY
;;       |  |      /  |   \              |
;;        \ |     /   |    \          VECTOR
;;       NULL    /    |     \            |
;;              /   FLOAT   COMPLEX   STRING
;;             /
;;         RATIONAL
;;         /      \
;;     INTEGER   RATIO
;;    /       \
;; FIXNUM    BIGNUM

;; the DEFSTRUCT macro
;; so, name, speed, condition, shields, these are all slots
(defstruct starship
  (name nil)
  (speed 0)
  (condition 'green)
  (shields 'down))

;; now we can reference it like so:
(defparameter sepulveda (make-starship))

(type-of sepulveda)
;; STARSHIP

(typep sepulveda 'starship)
;; T

;; it also generates accessors, which we can use as "place descriptions" for setf
(setf (starship-name sepulveda) "USSR SEPULVEDA")
(starship-name sepulveda)
;; "USSR SEPULVEDA"

;; also structures look like this when you print them down
sepulveda
;; #S(STARSHIP :NAME "USSR SEPULVEDA" :SPEED 0 :CONDITION GREEN :SHIELDS DOWN)

;; we can also do keyword args

(defun alert (starship)
  "puts a starship in 'alert' mode -- condition to yellow, shields up"
  (if (equal (starship-condition starship) 'green)
      (setf (starship-condition starship) 'yellow))
  (setf (starship-shields starship) 'up)
  'shields-raised)

(defvar BOI (make-starship :name "Buddypal"
			 :shields 'fucked
			 :condition 'bad))
BOI
;; #S(STARSHIP :NAME "Buddypal" :SPEED 0 :CONDITION BAD :SHIELDS FUCKED)

;; also re-defining a defstruct will really fuck things up
;; also sbcl straight-up won't let you do this.
;; (defstruct starship
;;   (captain nil)
;;   (name nil)
;;   (shields 'down)
;;   (speed 0)
;;   (condition 'green))

;; 12.1
;; CAPTAIN
;;   this symbol is used in the definition of a structure in the DEFSTRUCT macro
;;   this symbol is used to name the CAPTAIN slot of the STARSHIP structure
;; :CAPTAIN
;;   this symbol is used during the instantiation of a STARSHIP object
;;   this symbol is a keyword argument to the MAKE-STARSHIP function (constructor)
;; STARSHIP-CAPTAIN
;;   this symbol is the accessor function for the CAPTAIN slot on the STARSHIP structure
;;   this symbol is callable as a function, and provides a "place description" for
;;     the CAPTAIN slot on an instance of the STARSHIP structure 

;; 12.2
(starship-p 'starship)
					NIL;shouldn't be true

;; ? symbol
(type-of 'MAKE-STARSHIP)
;; SYMBOL

;; ? symbol
(type-of #'MAKE-STARSHIP)
;; COMPILED-FUNCTION ; I was not quite correct!

;; ? starship
(type-of (MAKE-STARSHIP))
;; STARSHIP

(describe sepulveda)
;; 
;; #S(STARSHIP..
  ;; [structure-object]
;; 
;; Slots with :INSTANCE allocation:
  ;; NAME                           = "USSR SEPULVEDA"
  ;; SPEED                          = 0
  ;; CONDITION                      = GREEN
  ;; SHIELDS                        = DOWN




