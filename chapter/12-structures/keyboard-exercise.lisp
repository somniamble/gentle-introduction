;; it's my new friend, the decision buddy!
;; a
(defstruct node
  name
  question
  (yes-case nil)
  (no-case nil))

;; b
(defparameter *node-list* nil)

(defun init ()
  (setf *node-list* nil))

;; c
(defun add-node (name question yes no)
  (push (make-node :name name
		   :question question
		   :yes-case yes
		   :no-case no)
	*node-list*))

;; d
(defun find-node (name)
  (find-if (lambda (node) (eq name (node-name node)))
	   *node-list*))

(init)

(add-node 'start
	  "Does ye engine turn over?"
	  'engine-turns-over
	  'engine-wont-turn-over)

(add-node 'engine-turns-over
	  "Will the engine run briefly for any period of time?"
	  'engine-runs-briefly
	  'engine-wont-run)

(add-node 'engine-wont-run
	  "IS there gas in the tank?"
	  'gas-in-tank
	  "Fill the tank and try starting the engine again")

(add-node 'engine-wont-turn-over
	  "Do you hear a sound when you turn the key?"
	  'sound-when-turn-key
	  'no-sound-when-turn-key)

(add-node 'no-sound-when-turn-key
	  "IS the battery voltage low?"
	  "Replace the battery"
	  'battery-voltage-ok)

(add-node 'battery-voltage-ok
	  "Are the wires crusty or loose?"
	  "Clean the battery cables and tighten the connections"
	  'battery-cables-good)


;; e
(defun prompt (text)
  "prompts for user input with text"
  (yes-or-no-p "~A" text))

(defun process-node (name)
  (let ((node (find-node name)))
    (if node
	(if (prompt (node-question node))
	    (node-yes-case node)
	    (node-no-case node))
	(format t "node ~S is not yet defined" name))))

;; f
(defun run ()
  "loops through the decision tree, prompting at each step, until we hit a terminal node"
  ;; the ~[~] directive is a conditional formatter
  ;; ~@[~] form of it will check if the first arg is non-nil
  ;;   if it is non-nil, then it backs up so that it can be used.
  (format t "~@[~A~]"
	  ;; we keep track of just the current leaf-value
	  (do ((curr 'start (process-node curr)))
	      ;; if the leaf is a string or nil, hand it back up to the format statement
	      ((or (stringp curr) (null curr)) curr))))

(defun prompt-repeatedly (prompt-text &key
					(read-function #'read)
					(test #'symbolp))
  (format t "~&~a > " prompt-text)
  (do ((value (funcall read-function)
	      (funcall read-function)))
      ((funcall test value) value)
    (format t "~&~S is an invalid entry, please try again" value)
    (format t "~&~a > " prompt-text)))

;; g
(defun add-node-interactive ()
  (flet ((is-valid-yesno (input)
	   (or (stringp input) (symbolp input))))
    (let ((name (prompt-repeatedly "please enter the name of the node"))
	  (question (prompt-repeatedly "please enter the text of the question"
				       :read-function #'read-line
				       :test #'stringp))
	  (yes (prompt-repeatedly "please enter the name of the yes-branch node"
				  :test #'is-valid-yesno))
	  (no (prompt-repeatedly "please enter the name of the no-branch node"
				 :test #'is-valid-yesno)))
      (add-node name question yes no))))

