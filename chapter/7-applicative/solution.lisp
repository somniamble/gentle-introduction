;; 7.10
(defvar note-table '((c 1)  (c-sharp 2)
		     (d 3)  (d-sharp 4)
		     (e 5)
		     (f 6)  (f-sharp 7)
		     (g 8)  (g-sharp 9)
		     (a 10) (a-sharp 11)
		     (b 12)))

(defun note-to-number (note)
  (cadr (assoc note note-table)))

(defun number-to-note (number)
  (car (find-if (lambda (x) (= x number))
		note-table :key #'cadr)))

(defun normalize-number (number)
  (+ 1 (mod (- number 1) 12)))

(defun numbers (notes)
  (mapcar #'note-to-number notes))

(defun notes (numbers)
  (mapcar #'number-to-note numbers))

;; d.
;; (NOTES (NOTES X

(defun raise (n numbers)
  (mapcar (lambda (x) (+ x n)) numbers))

(defun normalize (numbers)
  (mapcar #'normalize-number numbers))

(defun transpose (n song)
  (notes (normalize (raise n (numbers song)))))

(defvar mary-melody '(e d c d e e e))

(transpose 5 mary-melody)
;; (A G F G A A A)
(transpose 11 mary-melody)
;; (D-SHARP C-SHARP B C-SHARP D-SHARP D-SHARP D-SHARP)
(transpose 12 mary-melody)
;; (E D C D E E E)
(transpose -1 mary-melody)
;; (D-SHARP C-SHARP B C-SHARP D-SHARP D-SHARP D-SHARP)
(transpose -5 mary-melody)
;; (B A G A B B B)
(transpose 7 mary-melody)
;; (B A G A B B B)

;; 7.14
(defun my-setdiff (x y)
  (remove-if (lambda (e) (member e y))
	     x))

(defun my-intersection (x y)
  (remove-if-not (lambda (e)
		   (member e y))
		 x)
  ;; (remove-if-not (lambda (e)
  ;; 		   (and (member e x)
  ;; 			 (member e y)))
  ;; 		 x)
  )

(defun my-union (x y)
  (append
   (remove-if (lambda (e) (member e y))
	      x)
   y))

;; 7.15
(defvar my-hand
  '((3 hearts)
    (5 clubs)
    (2 diamonds)
    (4 diamonds)
    (ace spades)))

(defvar colors
  '((clubs black)
    (diamonds red)
    (hearts red)
    (spades black)))
;; a card looks like (<rank> <suit>)
;; <rank> is a number or a symbol 
;; <suit> is one of: spades, clubs, diamonds, hearts
(defun equaler (item &key (on #'eql))
  "makes a function of one argument that compares equality with the given item"
  (lambda (x) (funcall on item x)))

(defun rank (card)
  "gets the rank of a car"
  (car card))

(defun suit (card)
  "gets the suit of a card"
  (cadr card))

(defun count-suit (s hand)
  "counts the number of cards of a given suit in a hand"
  (length (remove-if-not (equaler s) hand :key #'suit)))

(defun color-of (card)
  "retrieves the color of a card based on its suit"
  (cadr (assoc (suit card) colors)))

;; d.
(defun first-red (hand)
  "retrieves the first red card from a hand"
  (find-if (equaler 'red) hand :key #'color-of))

;; e.
(defun black-cards (hand)
  "retrieves the list of black cards from a hand"
  (remove-if-not (equaler 'black) hand :key #'color-of))

;; f.
(defun what-ranks (s hand)
  "extracts the ranks belonging to a given suit in a hand"
  (mapcar #'rank (remove-if-not (equaler s) hand :key #'suit)))

;; g.
(defvar all-ranks
  '(2 3 4 5 6 7 8 9 10 jack queen king ace))

(defun higher-rank-p (c1 c2)
  "determines if card 1 has higher rank than card 2"
  (not
   (member (rank c2) (member (rank c1) all-ranks))))

;; h.
(defun pick-higher (c1 c2)
  "picks the highest of 2 cards"
  (if (higher-rank-p c1 c2) c1 c2))

(defun high-card (hand)
  "returns the highest ranked card in the hand"
  (reduce #'pick-higher hand))


;; 7.29

(defvar database
  '((b1 shape brick)
    (b1 color green)
    (b1 size small)
    (b1 supported-by b2)
    (b1 supported-by b3)
    (b2 shape brick)
    (b2 color red)
    (b2 size small)
    (b2 supports b1)
    (b2 left-of b3)
    (b3 shape brick)
    (b3 color red)
    (b3 size small)
    (b3 supports b1)
    (b3 right-of b2)
    (b4 shape pyramid)
    (b4 color blue)
    (b4 size large)
    (b4 supported-by b5)
    (b5 shape cube)
    (b5 color green)
    (b5 size large)
    (b5 supports b4)
    (b6 shape brick)
    (b6 color purple)
    (b6 size large)))

;; a
(defun match-element (sym1 sym2)
  "Matches two symbols, returning T if both are EQ or if sym2 is '?"
  (or (eq sym1 sym2) (eq sym2 '?)))


;; b
(defun match-triple (tr1 tr2)
  "Matches two triples; returns T if match-element across both triples are all T"
  (every 'match-element tr1 tr2))


;; c.
(defun fetch (pattern)
  "returns all assertions in the database that match PATTERN"
  (remove-if-not (lambda (triple)
		   (match-triple triple pattern))
		 database))

;; d.
;; what shape is b4?
(caddar (fetch '(b4 shape ?)))
;; PYRAMID

;; what blocks are bricks?
(mapcar 'car (fetch '(? shape brick)))
;; (B1 B2 B3 B6)

;; what relation is block b2 to block b3?
(fetch '(b2 ? b3))
;; ((B2 LEFT-OF B3))

;; color of every block
(fetch '(? color ?))
#+nil ((B1 COLOR GREEN)
       (B2 COLOR RED)
       (B3 COLOR RED)
       (B4 COLOR BLUE)
       (B5 COLOR GREEN)
       (B6 COLOR PURPLE))

;; what is known about b4?
(fetch '(b4 ? ?))
#+nil ((B4 SHAPE PYRAMID)
       (B4 COLOR BLUE)
       (B4 SIZE LARGE)
       (B4 SUPPORTED-BY B5))

;; e.
;; this is overengineered
(defun make-query (&key ((:block blk) '?) ((:feature feat) '?) ((:value val) '?))
  (list blk feat val))

(defun make-color-pattern (blk)
  "makes a query pattern for a block's color"
  (make-query :block blk :feature 'color))


;; f
(defun supporters (blk)
  "retrieves a list of all blocks that support blk"
  (mapcar 'car (fetch (list '? 'supports blk))))

;; g
(defun cubep (blk)
  (fetch (list blk 'shape 'cube)))

(defun supported-by-cube (blk)
  "determines if blk is supported by any blocks that are cubes"
  (find-if #'cubep (supporters blk)))

;; h.
(defun desc1 (blk)
  "gets a list of all assertions about a block"
  (fetch (list blk '? '?)))

;; i
(defun desc2 (blk)
  "strips the block name off of all facts known about a block"
  (mapcar #'cdr (desc1 blk)))

;; j
(defun description (blk)
  "returns a list describing all known features of blk"
  (apply #'append (desc2 blk)))



